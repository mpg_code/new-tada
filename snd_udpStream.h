#include <float.h>

int udp_fd_valid(int fd);
int getUdpStreamSpecifications(struct cmd_args *cmd_args);
int send_udpStream(struct cmd_args *cmd_args, int *currITT);
int write_udp_data(struct cmd_args *cmd_args);
int send_data(struct cmd_args *cmd_args, uint32_t size, double prPck_sTime, double *sTime);
int send_all(struct connectionInfo *ci, char *buf, int len, int *sent, int flags);