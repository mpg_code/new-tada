#include "tada.h"
#include "commonFunc.h"
#include "rcv_controlCH.h"
#include "tada_rcv.h"

/* Initialize Control Channel */
int initializeControlConn_rcv(struct cmd_args *cmd_args) {

	int i, len, rc, on = 1;
    int listen_sd, max_sd, new_sd;
    int desc_ready, end_server = FALSE;
    int close_conn;
    int status = 1;
    char buffer[80];
    struct timeval timeout;
    fd_set master_set, working_set;

    struct connectionInfo *conn = &cmd_args->controlConn;

	/* Open /dev/null to dispose of data */
	if ((conn->dev_null = fopen("/dev/null", "a")) == NULL) {
		printf("   Could not open /dev/null\n");
		return set_error_signal(cmd_args);
	}

	/* Create the TCP socket */
	//listen_sd = conn->listensock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	listen_sd = conn->listensock = socket(AF_INET, SOCK_STREAM, 0);
	if (conn->listensock < 0) {
		perror("   Failed to create TCP socket\n");
		return set_error_signal(cmd_args);
	}

	struct ifreq interface;

	/*if (cmd_args->interfaceOption == 1) {
		strncpy(interface.ifr_ifrn.ifrn_name, cmd_args->ifname, sizeof(cmd_args->ifname));
    	if (setsockopt(conn->listensock, SOL_SOCKET, SO_BINDTODEVICE,
        	               (char *)&interface, sizeof(interface)) < 0) {
        	perror("setting SO_BINDTODEVICE for UDP");
        	exit(1);
    	}

	}*/

	/* Construct the TCP server sockaddr_in structure */
	memset(&(conn->si_me), 0, sizeof(struct sockaddr_in));         /* Clear struct */
	conn->si_me.sin_family = AF_INET;                 /* Internet/IP */
	conn->si_me.sin_addr.s_addr = htonl(INADDR_ANY);  /* Incoming addr */
	conn->si_me.sin_port = htons(conn->server_port);  /* server port */

    int tr=1;
    if (setsockopt(listen_sd,SOL_SOCKET,SO_REUSEADDR,&tr,sizeof(int)) == -1)
        printf("   ERROR setting sock opt: %s\n",strerror(errno));

	/* Bind the TCP server socket */
	if (bind(conn->listensock,
		 (struct sockaddr *) &(conn->si_me), sizeof(struct sockaddr_in)) < 0) {
		perror("Failed to bind the TCP server socket\n");
		pthread_mutex_lock(&cmd_args->glblPars.unexpected_exit_mutex);
			cmd_args->glblPars.unexpected_exit = 1;
		pthread_mutex_unlock(&cmd_args->glblPars.unexpected_exit_mutex);

		return -1;
	}

	/* Listen on the server socket */
	if (listen(conn->listensock, MAXPENDING) < 0) {
		perror("Failed to listen on server socket\n");
		pthread_mutex_lock(&cmd_args->glblPars.unexpected_exit_mutex);
			cmd_args->glblPars.unexpected_exit = 1;
		pthread_mutex_unlock(&cmd_args->glblPars.unexpected_exit_mutex);

		return -1;
	}
	//system("ssh sender './snd -s 10.0.1.10 -u 12346 -d 5 -a &'");
	
	status = handle_new_controlConn(cmd_args,0);
	if (status >= 0) {
		cmd_args->glblPars.tada_phase = RATE_DETECTION_PHASE;
		printf("\n----------------------------------------\n\n");
		printf("\n   New Control Connection!\n");
		printf("----------------------------------------\n\n");
	} 
		

	// Set starting timestamp
	gettimeofday(&conn->start_time, NULL);	

	return 1;
}

int initializeUDPconn_rcv(struct cmd_args *cmd_args) {

	pthread_mutex_lock(&cmd_args->glblPars.unexpected_exit_mutex);
		if (cmd_args->glblPars.unexpected_exit == 1) {
			pthread_mutex_unlock(&cmd_args->glblPars.unexpected_exit_mutex);
			return -1;
		}
	pthread_mutex_unlock(&cmd_args->glblPars.unexpected_exit_mutex);

	struct connectionInfo *conn = &cmd_args->udpConn;
	/* Create the UDP socket*/
	conn->sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (conn->sock < 0) {
		printf("   Failed to create UDP socket");
		return -1;
	}

	struct ifreq interface;

	/*if (cmd_args->interfaceOption == 1) 
	{
		strncpy(interface.ifr_ifrn.ifrn_name, cmd_args->ifname, sizeof(cmd_args->ifname));
    	if (setsockopt(conn->sock, SOL_SOCKET, SO_BINDTODEVICE,
        	               (char *)&interface, sizeof(interface)) < 0) {
        	perror("setting SO_BINDTODEVICE for UDP");
        	exit(1);
    	}
	}*/

    

	/* Construct the TCP server sockaddr_in structure */
	memset(&(conn->si_me), 0, sizeof(struct sockaddr_in));         /* Clear struct */
	conn->si_me.sin_family = AF_INET;                 /* Internet/IP */
	conn->si_me.sin_addr.s_addr = htonl(INADDR_ANY);  /* Incoming addr */
	conn->si_me.sin_port = htons(conn->server_port);          /* server port */

	//Make UDP server non-blocking.
	int flags = fcntl(conn->sock, F_GETFL, 0);
	if (flags == -1) {
		printf("   fcntl\n");
		return -1;
	}

	if (fcntl(conn->sock, F_SETFL, O_NONBLOCK | flags) == -1) {
		printf("   fcntl\n");
		return -1;
	}

	/* Bind the UDP server socket */
	if (bind(conn->sock,
		 (struct sockaddr *) &(conn->si_me), sizeof(struct sockaddr_in)) < 0) {
		perror("   Failed to bind the UDP server socket\n");
		return -1;
	}

	// Set starting timestamp
	gettimeofday(&conn->start_time, NULL);
	return 1;
}

int resetRCV(struct cmd_args *cmd_args) {
	struct connectionInfo *udpConn_ci = &cmd_args->udpConn;
	struct connectionInfo *controlConn_ci = &cmd_args->controlConn;
	int rc, on = 1;

	memset(&cmd_args->glblPars, 0, sizeof(struct globalParameters));

	rc = fd_set_blocking(udpConn_ci->sock, 1);

	// Set UDP socket to be non-blocking.
	if (rc < 0)
	{
		perror("Error Setting UDP socket Blocking");
		return -1;
	}

	cmd_args->udpConn.strInfo.transmission_duration = cmd_args->sndSideTransmission_duration;

	return 1;	

}

void signal_handler(int sig) {

	if (sig > 0) {

		char signal_str[20] = {""};

		switch (sig) {
		case SIGINT:
			strcpy(signal_str, "SIGINT");
			break;
		case SIGTERM:
			strcpy(signal_str, "SIGTERM");
			break;
		default:
			strcpy(signal_str, "Unkown");
		}

		printf("\n   Caught signal %s\n", signal_str);

		//pthread_mutex_lock(&unexpected_exit_mutex);
		//	unexpected_exit = 1;
		//pthread_mutex_unlock(&unexpected_exit_mutex);

		return;
	}
}

