#include "tada.h"
#include "rcv_udpStream.h"
#include "commonFunc.h"

/* Functions */

void exitUdpRcvThread(struct cmd_args *cmd_args) {
	
	pthread_mutex_lock(&cmd_args->glblPars.unexpected_exit_mutex);
		if (cmd_args->glblPars.unexpected_exit == 1) {
			if (cmd_args->udpConn.strInfo.packet != NULL)
				free(cmd_args->udpConn.strInfo.packet);
		}
	pthread_mutex_unlock(&cmd_args->glblPars.unexpected_exit_mutex);
}

int updateUdpStream(struct streamInfo *stream, int packetCount, int packetSize, int packetITT) {
	stream->rcvPckCount = packetCount;
	stream->rcvPckITT_micros = packetITT;
	stream->rcvPckSize_byte = packetSize;
	stream->packet = (struct packetInfo*) calloc(sizeof(struct packetInfo), packetCount);
	return 1;
}

int initializeUdpStream(struct streamInfo *stream) {

	stream->lastSentSeqNr = 0;
	stream->bytes_sent = 0;
	stream->packets_sent = 0;

	stream->rcvPckCount = 0;
	stream->rcvPckITT_micros = 0;
	stream->rcvPckSize_byte = 0;
	stream->bytes_received = 0;
	stream->pckCountReceived = 0;
	stream->firstRcvSeqNr = 0;
	stream->lastRcvSeqNr = 0;
	stream->transmission_duration = 0;
	stream->min_OWD = 0;
	if (stream->packet != NULL)
		free(stream->packet);

	return 1;

}

int insertNewPck(struct streamInfo *stream, char *buffer, int buffer_size) {
	
	stream->bytes_received += buffer_size;
	stream->pckCountReceived += 1;

	struct timeval now;
	now.tv_sec = 0;
	now.tv_usec = 0;
	gettimeofday(&now, NULL);
	
	char seqNumberBfr[10], sentTimeBfr[100], prSentTimeBfr[100];
	memcpy(seqNumberBfr, buffer, 10);
	uint32_t seqNr = (uint32_t) atoi(seqNumberBfr);
	//memcpy(sentTimeBfr, buffer+11, 40);
	//double sentTime = (double) atof(sentTimeBfr);
	double *sentTime = (double*)&buffer[11];

	memcpy(prSentTimeBfr, buffer+52, 40);
	double prSentTime = (double) atof(prSentTimeBfr);
	
	stream->packet[seqNr].payloadSize = buffer_size;
	stream->packet[seqNr].seqNr = seqNr;
	tv2s(&now, &stream->packet[seqNr].receiveTime);
	stream->packet[seqNr].sendTime = *sentTime;
	stream->packet[seqNr].prSendTime = prSentTime;
	stream->packet[seqNr].oneWayDelay = stream->packet[seqNr].receiveTime - *sentTime;

	//printf("packet number %d sentTime %f receiveTime is %f\n", 
		//seqNr, *sentTime, stream->packet[seqNr].receiveTime);

	if (stream->pckCountReceived == 1) {
		stream->min_OWD = stream->packet[seqNr].oneWayDelay;
		stream->fastestPckSeqNr = seqNr;
		stream->firstRcvSeqNr = seqNr;
		stream->lastRcvSeqNr = seqNr;
	}
	else if (stream->pckCountReceived > 1) {
		if (seqNr > stream->lastRcvSeqNr+1 && stream->foundFirstLostSeqNr == 0) {
			stream->firstLostSeqNr = stream->lastRcvSeqNr+1;
			//printf("First Packet Loss: %d\n", stream->firstLostSeqNr);
			stream->foundFirstLostSeqNr = 1;
		}

		if (seqNr < stream->firstRcvSeqNr)
			stream->firstRcvSeqNr = seqNr;
		else
			stream->lastRcvSeqNr = seqNr;

		if (stream->packet[seqNr].oneWayDelay < stream->min_OWD) {
			stream->min_OWD = stream->packet[seqNr].oneWayDelay;
			if (stream->foundFirstLostSeqNr == 0)
				stream->fastestPckSeqNr = seqNr;

		}

	}

	return 1;
}

void *handle_udpThread(void *arg){

	int status = 1;
	int ok = 1;
	
	struct cmd_args *cmd_args = (struct cmd_args*) arg;

	// New packets for strInfo for udpStream for cmd_args;	

	ok = htonl(ok);
	if (send(cmd_args->controlConn.sock, &ok, sizeof(ok), 0) <= 0 ) {
       	perror("Send the OK message failed!!!");
       	status = -1; 
       	pthread_mutex_lock(&cmd_args->glblPars.unexpected_exit_mutex);
			cmd_args->glblPars.unexpected_exit = 1;
		pthread_mutex_unlock(&cmd_args->glblPars.unexpected_exit_mutex);       
    }		

    if (status >= 0) {
    	pthread_mutex_lock(&cmd_args->glblPars.udpthread_exit_mutex);
			cmd_args->glblPars.udpthread_exit = 0;
		pthread_mutex_unlock(&cmd_args->glblPars.udpthread_exit_mutex);

		status = startUdpRcv(cmd_args);
    }
	
	exitUdpRcvThread(cmd_args);

	pthread_mutex_lock(&cmd_args->glblPars.udpthread_exit_mutex);
		cmd_args->glblPars.udpthread_exit = 1;
	pthread_mutex_unlock(&cmd_args->glblPars.udpthread_exit_mutex);
	pthread_cond_broadcast(&cmd_args->glblPars.udpthread_exit_cond);
}

int startUdpRcv(struct cmd_args *cmd_args){

	int i, len, rc, on = 1;
    int listen_sd, max_sd;
    int desc_ready, end_server = FALSE;
    int close_conn;
    char buffer[80];
    struct timeval timeout;
    fd_set master_set, working_set;

	struct connectionInfo *conn = &cmd_args->udpConn;
	struct timeval last_UDPconnection;
	
	int end_while = 0;
	int res;
	int status = 1;

	listen_sd = conn->sock;

	//Allow socket descriptor to be reuseable
	rc = setsockopt(listen_sd, SOL_SOCKET,  SO_REUSEADDR,
	                (char *)&on, sizeof(on));
	if (rc < 0)
	{
		perror("setsockopt() failed");
		return -1;
	}

	// Set socket to be non-blocking.

	rc = ioctl(listen_sd, FIONBIO, (char *)&on);
	if (rc < 0)
	{
		perror("ioctl() failed");
		return -1;
	}

	/*************************************************************/
    /* Initialize the master fd_set                              */
    /*************************************************************/
    FD_ZERO(&master_set);
    max_sd = listen_sd;
    FD_SET(listen_sd, &master_set);

    /*************************************************************/
    /* Initialize the timeval struct to 1 seconds.  If no        */
    /* activity after 10 minutes this program will end.          */
    /*************************************************************/
    timeout.tv_sec  = 1;
    timeout.tv_usec = 0;



	do {

		pthread_mutex_lock(&cmd_args->glblPars.session_complete_mutex);
			if (cmd_args->glblPars.session_complete == 1) {
				status = 0;
			}
		pthread_mutex_unlock(&cmd_args->glblPars.session_complete_mutex);

		pthread_mutex_lock(&cmd_args->glblPars.unexpected_exit_mutex);
			if (cmd_args->glblPars.unexpected_exit == 1) {
				status = -1;
			}
		pthread_mutex_unlock(&cmd_args->glblPars.unexpected_exit_mutex);

		if (status <= 0)
			break;

		errno = 0;
		
		memcpy(&working_set, &master_set, sizeof(master_set));

    	rc = select(max_sd + 1, &working_set, NULL, NULL, &timeout);

    	if (rc < 0)
      	{
      		if (errno != EINTR)
        		perror("  select() failed");
        	break;
      	}

      	desc_ready = rc;

      	for (i=0; i <= max_sd  &&  desc_ready > 0; ++i)
      	{
      		if (FD_ISSET(i, &working_set)) 
      		{
      			desc_ready -= 1;
            	if (i == listen_sd) {
            		int status = handle_new_udpPck(conn);
            		if (status == -1) {
						fprintf(cmd_args->glblPars.logfile_rcv, "   Server has failed to accept a new UDP packet!!!\n");
						break;
					}
               		               		
            	}
            	
      		}
      	}

	} while (1);

	return status+1;
}

int handle_new_udpPck(struct connectionInfo *conn){

	socklen_t addr_size = sizeof(struct sockaddr_in);
	int res = 0;

	int sock = conn->sock;

	// new buffer in size of the packets

	char *buffer = (char*) malloc(MAX_PACKETSIZE_DEFAULT);
	int buffer_size = conn->strInfo.rcvPckSize_byte;
	int bytes_read = 0;

	int received = 0;	
	
	received = read_udpBytes(sock, buffer, buffer_size, &bytes_read, &conn->si_other, &addr_size);
	if (received) {
		res = insertNewPck(&conn->strInfo, buffer, received);
		if (res < 0) {
			printf("\n");
			printf("   Failed to insert new UDP packet into the stream array!\n");
			return -1;
		}	
	}

	return received;


}

int read_udpBytes(int sock, char *buf, int bytes_to_read, int *bytes_read, struct sockaddr_in *remote_addr, socklen_t *addr_size) {

	int rc = recvfrom(sock, buf, bytes_to_read, 0, (struct sockaddr *) remote_addr, addr_size);
	
	if (rc == 0) {
		// Client shutdown
		return 0;
	}
	else if (rc == -1) {
		// No more data available now
		if (errno == EAGAIN) {
			return -1;
		}
		else {
			printf("   Sock %d - recv returned -1 but EAGAIN is NOT set!\n", sock);
			perror("   perror recv");
			printf("   %d - read_bytes_raw return. Received: %d\n", sock, rc);
			return 0;
		}
	}
	return rc;
}