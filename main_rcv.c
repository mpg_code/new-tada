#include "tada.h"
#include "snd_controlCH.h"
#include "commonFunc.h"
#include "rcv_controlCH.h"
#include "rcv_parse_opts.h"
#include "tada_rcv.h"
#include <stdbool.h>
#include <sys/ioctl.h>

#define TIME_WAIT 1000

int receiving = 1; // 1 of currently receiving, else 0

int main(int argc, char *argv[]) {

	// Set up signal handler
	//struct sigaction new_action;

	struct cmd_args cmd_args;
	
	/* Set up the structure to specify the new action. */
	//new_action.sa_handler = &signal_handler;
	//sigemptyset(&new_action.sa_mask);
	//new_action.sa_flags = 0;
	//sigaction(SIGINT, &new_action, NULL);
	//sigaction(SIGHUP, &new_action, NULL);

	int status = 1;

	memset(&cmd_args, 0, sizeof(struct cmd_args));
	memset(&cmd_args.udpConn, 0, sizeof(struct connectionInfo));
	memset(&cmd_args.controlConn, 0, sizeof(struct connectionInfo));
	memset(&cmd_args.glblPars, 0, sizeof(struct globalParameters));

	cmd_args.glblPars.isReceiver = 1;
	cmd_args.glblPars.session_complete_mutex = PTHREAD_MUTEX_INITIALIZER;
	cmd_args.glblPars.unexpected_exit_mutex = PTHREAD_MUTEX_INITIALIZER;
	cmd_args.glblPars.udpthread_exit_mutex = PTHREAD_MUTEX_INITIALIZER;
	cmd_args.glblPars.udpthread_exit_cond = PTHREAD_COND_INITIALIZER;
	cmd_args.glblPars.signal_set_mutex = PTHREAD_MUTEX_INITIALIZER;

	cmd_args.glblPars.unexpected_exit = 0;
	cmd_args.glblPars.udpthread_exit = 1;
	cmd_args.glblPars.receiving = 1;

	parse_cmd_args_rcv(argc, argv, &cmd_args);
	//cmd_args.udpConn.server_port = 12346;
    //cmd_args.controlConn.server_port = 8181;
    //strcpy(cmd_args.glblPars.resultFolderName, "./results");
    //strcpy(cmd_args.glblPars.fileNamePrefix, "guiVersion");

    //mkpath(cmd_args.glblPars.resultFolderName);

    char filename[80];
    char logFileName[1500];
			
	/*Opening Log file for Receiving*/
	strcpy(filename, "logData_rcv");
	
	create_resultfile_name(filename, &cmd_args, logFileName, 1);

    if ((cmd_args.glblPars.logfile_rcv = fopen(logFileName, "w")) <= 0){
    	printf("   Could not Create Log File Output file - Error (%d): %s!!!\n", errno, strerror(errno));
    	status = -1;
    }

    //system("ssh sender './snd -s 10.0.1.10 -u 12346 -d 5 -a &' &");


	status = initializeControlConn_rcv(&cmd_args);

	//Initialize UDP server

	if (status == 1) {
		status = initializeUDPconn_rcv(&cmd_args);
		//printf("initializeUDPconn_rcv Done!\n");

	}
	if (status == 1){
		startRcvSideControlConn(&cmd_args);		
		
		pthread_mutex_lock(&cmd_args.glblPars.udpthread_exit_mutex);
			while (cmd_args.glblPars.udpthread_exit == 0)
				pthread_cond_wait(&cmd_args.glblPars.udpthread_exit_cond, &cmd_args.glblPars.udpthread_exit_mutex);
		pthread_mutex_unlock(&cmd_args.glblPars.udpthread_exit_mutex);
	}

	FILE *queueTypeFile;
			
	/*Opening Queue Type Result file*/
	strcpy(filename, qType_FILENAME);
	
	create_resultfile_name(filename, &cmd_args, cmd_args.glblPars.queueTypeFileName, 1);

    if ((queueTypeFile = fopen(cmd_args.glblPars.queueTypeFileName, "w")) <= 0){
    	printf("   Could not Create Queue Type Output file - Error (%d): %s!!!\n", errno, strerror(errno));
    	status = -1;
    }

    else {
    	fprintf(queueTypeFile, "%d\n", cmd_args.glblPars.currQueueType);
    	fclose(queueTypeFile);
    }
		

    

    switch (cmd_args.glblPars.currQueueType) {
		case QUEUETYPE_UNKNOWN:
			strcpy(cmd_args.finalQueueType, "Unknown");
			break;

		case QUEUETYPE_TD:
			strcpy(cmd_args.finalQueueType, "Tail-Drop");
			break;

		case QUEUETYPE_AQM:
			strcpy(cmd_args.finalQueueType, "AQM - Not able to detect the type");
			break;

		case QUEUETYPE_ARED:
			strcpy(cmd_args.finalQueueType, "ARED");
			break;

		case QUEUETYPE_CODEL:
			strcpy(cmd_args.finalQueueType, "CoDel");
			break;

		case QUEUETYPE_PIE:
			strcpy(cmd_args.finalQueueType, "PIE");
			break;
	}

	if (cmd_args.updownOption == 1) {
		/*Opening Log file for Sending */
		strcpy(filename, "logData_snd");
		status = 1;
	
		create_resultfile_name(filename, &cmd_args, logFileName, 1);

    	if ((cmd_args.glblPars.logfile_snd = fopen(filename, "w")) <= 0){
    		printf("   Could not Create Log File Output file!!!\n");
    		status = -1;
    	}
		cmd_args.glblPars.receiving = 0;
		cmd_args.glblPars.sending = 1;
		printf("\n   Starting DownStream Test!!\n\n");

		if (status == 1)
			status = resetRCV(&cmd_args);

		if (status > 0) {
			startSndSideControlCon(&cmd_args);
			fclose(cmd_args.glblPars.logfile_snd);
		}		

	}	

	close_program(&cmd_args);

    char c_logFileName[1500];
			
	/*Changing Log file name for Receiver*/	
	create_resultfile_name("logData_rcv", &cmd_args, c_logFileName, 1);
	int ret = rename(logFileName, c_logFileName);

	if (ret == 0)
		printf("   File renamed successfully!!!\n");

	fclose(cmd_args.glblPars.logfile_rcv);

	return 0;
		
}
