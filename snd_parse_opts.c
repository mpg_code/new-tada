#include "tada.h"
#include "snd_parse_opts.h"
#include "commonFunc.h"

void required_argument_check(char *optarg, struct cmd_args *cmd_args, char arg) {
	if (!optarg || optarg[0] == '-') {
		//printf("Argument required for option '%c'\n", arg);
		usage(cmd_args);
		exit(1);
	}
}

void usage(struct cmd_args *cmd_args) {

	if (cmd_args->help) {
		printf("Usage: %s %s\n", cmd_args->program_name, USAGE_STR);
		printf("Required options:\n");
	}
	
	printf(" -s <servername>                : Server to connect to.\n");
	printf(" -u <udpPortNumber>             : UDP port to connect to.\n\n");


	printf("Upstream options:\n");
	printf(" -I <interfacename>             : interface to send to.\n");
	printf(" -a                             : Automatic test\n");
	printf(" -i <InterTransmissionTime>     : Packets ITT (non-auto test), in micro seconds.(Dfeault 0)\n");
	printf(" -p <PacketNumber>              : Number of UDP Stream Packet (auto and non-auto). (Default 2000)\n");
	printf(" -m <PacketSize>                : Packet Size (auto and non-auto) in Byte. (Default 1472B)\n");
	printf(" -d <testDuration>              : Test Duration (auto and non-auto) in Seconds. (Default 7)\n");
	printf(" -c <ControlChannelPortNumber>  : Control port to connect to\n");
	printf(" -f <channelErrorFile>          : Prepared File for Emulating Channel Error.\n");
	printf(" -h                             : Help\n\n");

	printf("  -T                             : Up/Down Stream Queue Detection\n");
	printf("Downstream options:\n");
	printf("  -P <namePrefixStream>          : Name Prefix for Result Files: Default is empty\n");
	printf("  -o <outputFolderName>          : Result Folder Name: Default is ./result\n");
	printf("  -r                             : Keep Packets Receive Time\n\n");

	
	

}

void parse_cmd_args_snd(int argc, char *argv[], struct cmd_args *cmd_args) {

	int arg_off = 0;
	int opt;
	int option_index = 0;
	int c;
	cmd_args->udpConn.dev_null = NULL;
	cmd_args->controlConn.dev_null = NULL;
	cmd_args->udpConn.strInfo.packet = NULL;
	cmd_args->controlConn.strInfo.packet = NULL;
	char *servername;

	FILE *tTableFile;
	char comma;
	int i, j;
	char str[50];

	while (1) {
		c = getopt(argc, argv, OPTSTRING);

		if (c == -1)
			break;

		switch (c) {

		case 's' :
			required_argument_check(optarg, cmd_args, c);
			servername = optarg;
			break;

		case 'u' :
			required_argument_check(optarg, cmd_args, c);
			cmd_args->udpConn.server_port = atoi(optarg);		
			break;

		case 'I' :
			required_argument_check(optarg, cmd_args, c);
			cmd_args->ifname = optarg;
			cmd_args->interfaceOption = 1;
			break;			

		case 'c' :
			required_argument_check(optarg, cmd_args, c);
			cmd_args->controlConn.server_port = atoi(optarg);
			break;

		case 'h' :
			cmd_args->help = 1;
			break;
		
		case 'a' :
			cmd_args->is_automatic_test = 1;
			arg_off++;
			break;

		case 'i': //intertransmissiontime
			required_argument_check(optarg, cmd_args, c);
			cmd_args->udpConn.strInfo.sentPckITT_micros = next_int(&optarg);
			break;

		case 'p': //packet number
			required_argument_check(optarg, cmd_args, c);
			cmd_args->udpConn.strInfo.sentPckCount = next_int(&optarg);
			break;

		case 'm':
			required_argument_check(optarg, cmd_args, c);
			cmd_args->udpConn.strInfo.sentPckSize_byte = next_int(&optarg);
			break;

		case 'd':
			required_argument_check(optarg, cmd_args, c);
			cmd_args->udpConn.strInfo.transmission_duration = next_int(&optarg);
			break;

		case 'f':
			required_argument_check(optarg, cmd_args, c);
			cmd_args->glblPars.channelError = 1;
			strcpy(cmd_args->chennelErrorFile,optarg);
			break;

		case 'T' :
			cmd_args->updownOption = 1;
			arg_off++;
			break;

		case 'P':
			required_argument_check(optarg, cmd_args, c);
			strcpy(cmd_args->glblPars.fileNamePrefix, optarg);
			arg_off += 2;
			break;

		case 'r' :
			cmd_args->pckReceiveTime = 1;
			arg_off++;
			break;

		case 'o':
			required_argument_check(optarg, cmd_args, c);
			strcpy(cmd_args->glblPars.resultFolderName, optarg);
			arg_off += 2;
			break;
			
		case '?' :
			//printf("Unknown option: '%c'\n", c);
			usage(cmd_args);
			exit(1);

		default :
			break;
		}
	}

	if (servername == 0 || cmd_args->udpConn.server_port == 0) {
		//printf("Error: You must specify servername and udp port!\n");
		usage(cmd_args);
		exit(1);
	}

	if (servername != 0) {
		if ((cmd_args->controlConn.host = cmd_args->udpConn.host = (struct hostent*) gethostbyname(servername)) == NULL) {
			//printf("Failed to lookup host:%s\n", servername);
			exit(1);
		}
	}


	if (cmd_args->controlConn.server_port == 0)
		cmd_args->controlConn.server_port = DEFAULT_CONTROLCH_PORT;

	if (cmd_args->udpConn.strInfo.sentPckSize_byte == 0) {
			cmd_args->udpConn.strInfo.sentPckSize_byte = MAX_PACKETSIZE_DEFAULT;
	}

	if (cmd_args->udpConn.strInfo.sentPckCount == 0 && 
		cmd_args->udpConn.strInfo.transmission_duration == 0) {
		cmd_args->udpConn.strInfo.transmission_duration = 0;
		cmd_args->udpConn.strInfo.sentPckCount = DEFAULT_PACKET_COUNT;
	}
	else if (cmd_args->udpConn.strInfo.sentPckCount != 0 && 
		cmd_args->udpConn.strInfo.transmission_duration != 0) {
		//printf("Error: Can not specify both the number of packets and the test Duration!\n");
		usage(cmd_args);
		exit(1);
	}







}
