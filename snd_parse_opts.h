#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <libgen.h>

#define OPTSTRING "s:u:I:c:p:i:m:d:P:o:f:rahT"
#define USAGE_STR "-s -u [-IaipmdchTPorc]"

void usage(struct cmd_args *cmd_args);
void parse_cmd_args_snd(int argc, char *argv[], struct cmd_args *cmd_args);
void required_argument_check(char *optarg, struct cmd_args *cmd_args, char arg, char *argv);