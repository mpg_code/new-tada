#include "tada.h"
#include "snd_controlCH.h"
#include "commonFunc.h"
#include "rcv_controlCH.h"
#include "snd_parse_opts.h"
#include "tada_snd.h"
#include "tada_rcv.h"


int main(int argc, char *argv[]) {

	int status = 1;
	FILE *errorInput;

	struct cmd_args cmd_args;

	memset(&cmd_args, 0, sizeof(struct cmd_args));
	memset(&cmd_args.udpConn, 0, sizeof(struct connectionInfo));
	memset(&cmd_args.controlConn, 0, sizeof(struct connectionInfo));
	memset(&cmd_args.glblPars, 0, sizeof(struct globalParameters));

	/* Parse command line arguments */
	parse_cmd_args_snd(argc, argv, &cmd_args);

	cmd_args.glblPars.session_complete_mutex = PTHREAD_MUTEX_INITIALIZER;
	cmd_args.glblPars.unexpected_exit_mutex = PTHREAD_MUTEX_INITIALIZER;
	cmd_args.glblPars.udpthread_exit_mutex = PTHREAD_MUTEX_INITIALIZER;
	cmd_args.glblPars.udpthread_exit_cond = PTHREAD_COND_INITIALIZER;

	cmd_args.glblPars.unexpected_exit = 0;
	cmd_args.glblPars.udpthread_exit = 1;

	if(cmd_args.glblPars.channelError == 1) {
		if ((errorInput = fopen(cmd_args.chennelErrorFile, "r")) <= 0){
    		//printf("Could not open ChannelError File!!!\n");
    		//printf("Exiting the Program\n");
    		return ERROR;
    	}
    	int number = 0;
    	while (!feof(errorInput)) {
    		cmd_args.glblPars.channelErrorArraySize++;
    		fscanf(errorInput, "%d\n", &number);
    	}
    	cmd_args.glblPars.channelErrorArray = (int*) calloc(sizeof(int), cmd_args.glblPars.channelErrorArraySize);

    	fseek(errorInput, SEEK_SET, 0);

    	int i = 0;
    	while (!feof(errorInput)) {
    		fscanf(errorInput, "%d\n", &number);
    		cmd_args.glblPars.channelErrorArray[i] = number;
    		i++;
    	}

    	fclose(errorInput);

	}

	char filename[80];
    char logFileName[500];
			
	/*Opening Log file for Receiving*/
	strcpy(filename, "logData_snd");
	
	create_resultfile_name(filename, &cmd_args, logFileName, 1);

    if ((cmd_args.glblPars.logfile_snd = fopen(filename, "w")) <= 0){
    	printf("   Could not Create Log File Output file!!!\n");
    	status = -1;
    }
	
	/* Start Control Channel First */
	status = initializeControlConn_snd(&cmd_args);

	/* Start UDP Connection*/
	if (status != -1) {
		//printf("Starting initializeUDPconn()!\n");
		status = initializeUDPconn_snd(&cmd_args);
	}

	if (status != -1) {
		//printf("Starting startSndSideControlCon()!\n");
		status = startSndSideControlCon(&cmd_args);
	}

	
	if (status != 1 && cmd_args.updownOption == 1) {
		//printf("Starting DownStream Test!!\n");

		status = resetSND(&cmd_args);

		/*Opening Log file for Receiving*/
		strcpy(filename, "logData_rcv");
	
		create_resultfile_name(filename, &cmd_args, logFileName, 1);

    	if ((cmd_args.glblPars.logfile_rcv = fopen(filename, "w")) <= 0){
    		printf("   Could not Create Log File Output file!!!\n");
    		status = -1;
    	}

		if (status != -1) {
			startRcvSideControlConn(&cmd_args);
		
			pthread_mutex_lock(&cmd_args.glblPars.udpthread_exit_mutex);
				while (cmd_args.glblPars.udpthread_exit == 0)
					pthread_cond_wait(&cmd_args.glblPars.udpthread_exit_cond, &cmd_args.glblPars.udpthread_exit_mutex);
			pthread_mutex_unlock(&cmd_args.glblPars.udpthread_exit_mutex);

			FILE *queueTypeFile;
			
			/*Opening Queue Type Result file*/
			strcpy(filename, qType_FILENAME);
			create_resultfile_name(filename, &cmd_args, cmd_args.glblPars.queueTypeFileName, 1);

    		if ((queueTypeFile = fopen(cmd_args.glblPars.queueTypeFileName, "w")) <= 0){
    			//printf("Could not Create Queue Type Output file!!!\n");
    			status = -1;
    		}

    		fprintf(queueTypeFile, "%d\n", cmd_args.glblPars.currQueueType);

    		fclose(queueTypeFile);
    		fclose(cmd_args.glblPars.logfile_rcv);
		}


	}

	close_program(&cmd_args);
	fclose(cmd_args.glblPars.logfile_snd);

	
}