#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "gnuplot_i.h"

#define DEFAULT_QUEUETIME_XLABEL "Packet Nr"
#define DEFAULT_QUEUETIME_YLABEL "Queueing Delay[ms]"
#define DEFAULT_QUEUETIME_COLOR "blue"
#define DEFAULT_LOSSNUMBER_COLOR "red"
#define DEFAULT_QUEUETIME_TITLE "Queueing Delay For Delivered Packets"
#define DEFAULT_LOSSNUMBER_TITLE "Lost Packets"

int find_min(double *data, int start, int end, double *min);
int sort(int number, double *array);
void slope_calculator(int *x, double *y, int start, int end, 
	double minY, double *slope);
int set_error_signal(struct cmd_args *cmd_args);
void calculate_standardDeviation(double *data, int start, int size, double *sd, double *avg);
void calculate_standardError(int n1, int n2, double s1, double s2, double *se);
void calculate_pValue(int n1, int n2, double avg1, double avg2, double se, double *pValue);
void calculate_confidenceInterval(int n1, int n2, double avg1, double avg2, double se,
	double *upperB, double *lowerB);
int find_group_size(int start, int mainSize);
void copyArray(double *array1, double *array2, int array2Size);
int fd_set_blocking(int fd, int blocking);
void close_program(struct cmd_args *cmd_args);
void create_resultfile_name(char *filename, struct cmd_args *cmd_args, char *resultFileName, int isQueueType);
void time_to_string(struct cmd_args *cmd_args, char *tmbuf);
void mkpath(const char *dir);
void tv2s(const struct timeval *tv, double *tValue);
double tv2ms(const struct timeval *tv);
double tv2micros(const struct timeval *tv);
void tv_divide(const unsigned long divisor, const struct timeval *tv, struct timeval *result);
void tv_scale(const unsigned long mult, const struct timeval *tv, struct timeval *result);
void required_argument_check(char *optarg, struct cmd_args *cmd_args, char arg, char *argv);
void skip(char **str, char s, char option);
long next_digit(char *str, char **endptr, long *result);
long next_int(char **str);
void deplete_sendbuffer(struct connectionInfo* ci);
int receiveMessage(int sock);
int queueTime_lossDensity_plot(char *plotFileName, char *queueTime, 
	char *lossNr, char *lossDensity, int test_duration, 
	double max_qTime, double frstLssNr_Time, double maxPckNr_Time);
