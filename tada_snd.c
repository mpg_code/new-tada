#include <assert.h>

#include "tada.h"
#include "commonFunc.h"
#include "tada_snd.h"

/* check whether a file-descriptor is valid */
int tcp_fd_valid(int fd) {
	if (fcntl(fd, F_GETFL) == -1 && errno == EBADF)
		return 0;
	return 1;
}

/*
   Sets up a TCP connection for The Control Channel
*/

int initializeControlConn_snd(struct cmd_args *cmd_args){
	int sock;
	struct sockaddr_in server, client;
	int client_port = CONTROLCH_START_PORT;
	int rc,on = 1;

	struct connectionInfo *connInfo = &cmd_args->controlConn;

	/* Initialize rand() */
	srand48((unsigned)time(NULL));

	/* Construct the server sockaddr_in structure */
	memset(&connInfo->si_other, 0, sizeof(struct sockaddr_in));       /* Clear struct */
	connInfo->si_other.sin_family = AF_INET;                  /* Internet/IP */
	connInfo->si_other.sin_addr.s_addr = *((unsigned long *) connInfo->host->h_addr_list[0]); /* IP address */
	connInfo->si_other.sin_port = htons(DEFAULT_CONTROLCH_PORT);       /* server port */
		/* Create the socket */
	sock = connInfo->sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sock < 0) {
		printf("   Control Channel Connection Failed: %s\n",strerror(errno));
		fprintf(cmd_args->glblPars.logfile_snd, "   Control Channel Connection Failed: %s\n",strerror(errno));
		return ERROR;

	}

	struct ifreq interface;

	 int tr=1;
    if (setsockopt(sock,SOL_SOCKET,SO_REUSEADDR,&tr,sizeof(int)) == -1)
        printf("   ERROR setting sock opt: %s\n",strerror(errno));

	/*if (cmd_args->interfaceOption == 1) 
	{
		strncpy(interface.ifr_ifrn.ifrn_name, cmd_args->ifname, sizeof(cmd_args->ifname));
    	if (setsockopt(sock, SOL_SOCKET, SO_BINDTODEVICE,
        	               (char *)&interface, sizeof(interface)) < 0) {
        	perror("setting SO_BINDTODEVICE for UDP");
        	exit(1);
    	}	
	}*/    

	/* Specify outgoing port to avoid reusing ports (which makes analysis more complicated */
	/* Construct the server sockaddr_in structure */
	memset(&client, 0, sizeof(struct sockaddr_in));          /* Clear struct */
	connInfo->si_me.sin_family = AF_INET;                  /* Internet/IP */
	connInfo->si_me.sin_addr.s_addr = htonl(INADDR_ANY);   /* addr */
	connInfo->si_me.sin_port = htons(DEFAULT_CONTROLCH_PORT);

	int err = -1;
	int portAttempts = 0;
	while ((portAttempts < 100) && (err < 0)) {
		err = bind(sock, (struct sockaddr *) &connInfo->si_me, sizeof(struct sockaddr_in));
		if (err < 0) {

			/* Try to change conn port */
			client_port += 1;
			client.sin_port = htons(client_port);
			portAttempts++;
			struct timespec req;
			req.tv_sec = 0;
			req.tv_nsec = 1000000 * 10; // Sleep 10 milliseconds
			nanosleep(&req, NULL);
		}
	}

	if (err < 0) {
		perror("Failed to assign Control Channel port in 100 attempts!!!\n");
		return ERROR;

	}

	assert(err != -1);

	/* Establish connection */
	int connAttempts = 5;
	err = -1;
	while ((connAttempts > 0) && (err < 0)) {
		err = connect(sock, (struct sockaddr *) &connInfo->si_other, sizeof(struct sockaddr_in));
		if (err < 0) {
			connAttempts--;

			/* Try again in 3 seconds */
			struct timespec req;
			req.tv_sec = 3;
			req.tv_nsec = 0;
			nanosleep(&req, NULL);
		}
	}

	if (err < 0) {
		printf("Contorl Channel Failed to connect to server on 5 attempts: %s\n",strerror(errno));
		fprintf(cmd_args->glblPars.logfile_snd, "Contorl Channel Failed to connect to server on 5 attempts: %s\n",strerror(errno));
		return ERROR;

	}
	else {
		fprintf(cmd_args->glblPars.logfile_snd, "\n----------------------------------------\n");
		fprintf(cmd_args->glblPars.logfile_snd, "   Control Channel Connected!\n");
		fprintf(cmd_args->glblPars.logfile_snd, "----------------------------------------\n\n");
	}

	assert(err != -1);

	struct pollfd pfd;
	pfd.fd = sock;
	pfd.events = POLLERR;

	nfds_t n = 1;
	if (poll(&pfd, n, 0) < 0) {
		fprintf(cmd_args->glblPars.logfile_snd, "Socket error!\n");
	}

	if (pfd.revents & POLLERR) {
		fprintf(cmd_args->glblPars.logfile_snd, "pipe is broken\n");
	}

	if (!tcp_fd_valid(sock)) {
		fprintf(cmd_args->glblPars.logfile_snd, "Socket NOT valid!\n");
	}

	//TODO: Call The UDP Function to Send the Data

	return 1;
}

int initializeUDPconn_snd(struct cmd_args *cmd_args) {

	int sock;
	int i;

	struct connectionInfo *connInfo = &cmd_args->udpConn;

	/* Initialize rand() */
	srand48((unsigned)time(NULL));

	/* Construct the server sockaddr_in structure */
	memset(&connInfo->si_other, 0, sizeof(struct sockaddr_in));       /* Clear struct */
	connInfo->si_other.sin_family = AF_INET;                  /* Internet/IP */
	connInfo->si_other.sin_addr.s_addr = *((unsigned long *) connInfo->host->h_addr_list[0]); /* IP address */
	connInfo->si_other.sin_port = htons(connInfo->server_port);       /* server port */

	/* Create the socket */
	connInfo->sock = sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock < 0) {
		fprintf(cmd_args->glblPars.logfile_snd, "Failed to create an UDP socket\n");
		return ERROR;
	}

    struct ifreq interface;

	/*if (cmd_args->interfaceOption == 1) 
	{
		strncpy(interface.ifr_ifrn.ifrn_name, cmd_args->ifname, sizeof(cmd_args->ifname));
    	if (setsockopt(sock, SOL_SOCKET, SO_BINDTODEVICE,
        	               (char *)&interface, sizeof(interface)) < 0) {
        	perror("setting SO_BINDTODEVICE for UDP");
        	exit(1);
    	}

	}*/

	/* Construct the server sockaddr_in structure */
	memset(&connInfo->si_me, 0, sizeof(struct sockaddr_in));          /* Clear struct */
	connInfo->si_me.sin_family = AF_INET;                  /* Internet/IP */
	connInfo->si_me.sin_addr.s_addr = htonl(INADDR_ANY);   /* addr */

	connInfo->client_port = UDPSTREAM_START_PORT;
	connInfo->si_me.sin_port = htons(connInfo->client_port);

	int err = -1;
	int portAttempts = 0;
	while ((portAttempts < 100) && (err < 0)) {
		err = bind(sock, (struct sockaddr *) &connInfo->si_me, sizeof(struct sockaddr_in));
		if (err < 0) {

			connInfo->client_port+=1;
			connInfo->si_me.sin_port = htons(connInfo->client_port);
			portAttempts++;
			struct timespec req;
			req.tv_sec = 0;
			req.tv_nsec = 1000000 * 10; // Sleep 10 milliseconds
			nanosleep(&req, NULL);
		}
	}

	if (err < 0) {
		perror("Failed to assign UDP port in 100 attempts\n");
		return ERROR;
	}

	assert(err != -1);

	return 1;

}

int resetSND(struct cmd_args *cmd_args) {
	struct connectionInfo *udpConn_ci = &cmd_args->udpConn;
	struct connectionInfo *controlConn_ci = &cmd_args->controlConn;
	int rc, on = 1;

	memset(&cmd_args->glblPars, 0, sizeof(struct globalParameters));
	cmd_args->glblPars.currQueueType = -1;

	mkpath(cmd_args->glblPars.resultFolderName);

	// Set UDP socket to be non-blocking.
	rc = fd_set_blocking(udpConn_ci->sock, 0);

	// Set UDP socket to be non-blocking.
	if (rc < 0)
	{
		perror("Error Setting UDP socket Blocking");
		return ERROR;
	}

	pthread_mutex_lock(&cmd_args->glblPars.unexpected_exit_mutex);
		cmd_args->glblPars.unexpected_exit = 0;
	pthread_mutex_unlock(&cmd_args->glblPars.unexpected_exit_mutex);

	pthread_mutex_lock(&cmd_args->glblPars.session_complete_mutex);
		cmd_args->glblPars.session_complete = 0;
	pthread_mutex_unlock(&cmd_args->glblPars.session_complete_mutex);

	return 1;	

}