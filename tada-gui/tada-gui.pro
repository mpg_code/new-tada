#-------------------------------------------------
#
# Project created by QtCreator 2016-03-31T13:36:27
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = tada-gui
TEMPLATE = app
CONFIG += c++11

CONFIG += -g


SOURCES += main.cpp\
        mainwindow.cpp \
    test.cpp \
    waitsignalthread.cpp

HEADERS  += mainwindow.h \
    test.h \
    waitsignalthread.h



INCLUDEPATH += ../../Qt/qwt-6.1/src
INCLUDEPATH += ../../new-tada

LIBS += -L../../Qt/qwt-6.1/lib -lqwt
LIBS += -lpthread -lm -L../../new-tada -ltada_rcv
