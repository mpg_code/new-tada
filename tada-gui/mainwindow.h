#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <qwt_plot.h>
#include <qwt_plot_multi_barchart.h>
#include <qwt_plot_barchart.h>
#include <QComboBox>
#include <qwt_plot.h>
#include <qwt_plot_spectrocurve.h>
#include <QVector>
#include <qwt_point_3d.h>
#include <qwt_scale_engine.h>
#include <QPushButton>
#include <QLineEdit>
#include <QThread>
#include <QMutex>
#include <QProgressBar>
#include <string>
#include <vector>

#include "test.h"

class Test;

class MainWindow : public QWidget
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);

    ~MainWindow();
signals:
    void scheduleReplot();
public slots:
    void updateIntermediateResult();
    void prepareNewTest();
    void startTest();
    void stopTest();
    void testCompleted();
    void commitData();
    void updateQType1(int index);
    void updateQType2(int index);
    void updateBWLim1(int index);
    void updateBWLim2(int index);
    void updateBufLim1(int index);
    void updateBufLim2(int index);
    void updateNrStreams(int index);
private:

    void updatePlot();
    QwtPlot *plotQD;
    QwtPlot *plotLD;

    QwtPlotSpectroCurve *scatterQD;
    QwtPlotSpectroCurve *scatterLD;

    QVector<QwtPoint3D> plotDataQD;
    QVector<QwtPoint3D> plotDataLD;
    QPushButton *bStart;
    QPushButton *bEnd;
    QLineEdit *tCtrlChannelPort;
    QLineEdit *tITT;
    QLineEdit *tPacketNr;
    QLineEdit *tPacketSz;
    QLineEdit *tTestDuration;

    QComboBox *tFirstRouterQtype;
    QComboBox *tFirstRouterBWLimit;
    QComboBox *tFirstRouterBufLimit;
    QComboBox *tSecondRouterQtype;
    QComboBox *tSecondRouterBWLimit;
    QComboBox *tSecondRouterBufLimit;
    QComboBox *cNrStreams;

    QLineEdit *tlQtype;
    QLineEdit *tlTestStatus;

    QMutex dataMutex;
    QMutex paramMutex;

    QProgressBar *pTestProgress;


    int cport;
    int itt;
    int pnr;
    int psz;
    int tdur;
    std::string qtype1;
    std::string bwlim1;
    std::string buflim1;
    std::string qtype2;
    std::string bwlim2;
    std::string buflim2;
    int nrstreams;

    Test *t;
    QThread *testThread;
    result res;

    std::vector<std::string> qtypes;
    std::vector<std::string> bwlimits2;
    std::vector<std::string> bwlimits1;
    std::vector<std::string> buflimits;
    std::string qtype_out;
    std::string test_status;

};


#endif // MAINWINDOW_H
