#include "waitsignalthread.h"
#include <iostream>
#include <QThread>
#include <QTimer>

WaitSignalThread::WaitSignalThread(pthread_mutex_t* lock, pthread_cond_t *cond, bool *testdone, Test* t)
    : m_lock(lock), m_cond(cond), m_done(testdone), m_tobj(t)
{

}

void WaitSignalThread::start()
{

    bool done = *m_done;
    if(!done) {
        std::cout << "waiting on condition" << std::endl;
        pthread_mutex_lock(m_lock);
        pthread_cond_wait(m_cond, m_lock);
        std::cout << "got a signal" << std::endl;

        done = *m_done;
        pthread_mutex_unlock(m_lock);
        m_tobj->intermediateResult();
    } else {
        QThread *mythread = this->thread();
        std::cout << "exiting thread waitSignalThread" << std::endl;
        mythread->exit();
    }
    start();
}


