#ifndef TEST_H
#define TEST_H

#include "tada.h"
#include "tada_rcv.h"

#include "commonFunc.h"
#include "snd_controlCH.h"
#include "rcv_controlCH.h"
#include "mainwindow.h"


#include <QObject>
#include <QVector>
#include <qwt_point_3d.h>
#include <string>
#include <QThread>
class MainWindow;

class Test: public QObject
{
    Q_OBJECT
public:
    explicit Test(MainWindow *m
                  , result *res
                  , int cport
                  , int itt
                  , int pnr
                  , int psz
                  , int tdur
                  , std::string qtype1
                  , std::string bwlim1
                  , std::string buflim1
                  , std::string qtype2
                  , std::string bwlim2
                  , std::string buflim2
                  , int nrstreams);
struct result getOutput();
signals:
   void testDone();
   void resultReady();

public slots:
    void start();
    void stop();
    void startReceiver();
    void intermediateResult();
private:
    void configureRouters();
    void startBackgroundTraffic();
    void outputData();
    struct cmd_args cmd_args;
    result *res_out;
    QVector<QwtPoint3D> *plotDataQD;
    QVector<QwtPoint3D> *plotDataLD;
    std::string *m_qtype_out;
    int m_cport;
    int m_itt;
    int m_pnr;
    int m_psz;
    int m_tdur;
    int m_nrstreams;
    std::string m_qtype1;
    std::string m_bwlim1;
    std::string m_buflim1;
    std::string m_qtype2;
    std::string m_bwlim2;
    std::string m_buflim2;
    MainWindow *mv;

};

#endif // TEST_H
