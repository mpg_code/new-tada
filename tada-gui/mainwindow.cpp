#include "mainwindow.h"


#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <qwt_color_map.h>
#include <iostream>

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
{
    this->setMinimumWidth(900);
    this->setMinimumHeight(600);
    this->setWindowTitle("TADA");

    QVBoxLayout *mainLayout = new QVBoxLayout;
    setLayout(mainLayout);
    QHBoxLayout *upperMenu = new QHBoxLayout;
    QHBoxLayout *upperMenu1 = new QHBoxLayout;
    QHBoxLayout *upperMenu2 = new QHBoxLayout;
    QHBoxLayout *upperMenu3 = new QHBoxLayout;

    QHBoxLayout *detectedQtype = new QHBoxLayout;
    QHBoxLayout *status = new QHBoxLayout;
    QHBoxLayout *progress = new QHBoxLayout;

    mainLayout->addLayout(upperMenu);
    mainLayout->addLayout(upperMenu1);
    mainLayout->addLayout(upperMenu2);
    mainLayout->addLayout(upperMenu3);

    mainLayout->addLayout(status);
    mainLayout->addLayout(progress);

    mainLayout->addLayout(detectedQtype);
    QHBoxLayout *plots = new QHBoxLayout;
    mainLayout->addLayout(plots);


  /*  tCtrlChannelPort = new QLineEdit("8181", this);
    tITT = new QLineEdit("8181", this);
    tPacketNr = new QLineEdit("8181", this);
    tPacketSz = new QLineEdit("8181", this);
    tTestDuration = new QLineEdit("8181", this);*/

    bStart = new QPushButton(this);
    bStart->setText("Start test");
    bStart->setFocus();
    bEnd = new QPushButton(this);
    bEnd->setText("Stop test");
    bStart->setEnabled(true);
    bEnd->setEnabled(false);

 /*   QLabel *lCtrlPort = new QLabel("Control Channel Port:");
    QLabel *lITT = new QLabel("ITT:");
    QLabel *lPacketNr = new QLabel("Packet nr:");
    QLabel *lPacketSz = new QLabel("Packet size:");
    QLabel *lTestDuration = new QLabel("Test duration:");*/

 /*   upperMenu->addWidget(lCtrlPort);
    upperMenu->addWidget(tCtrlChannelPort);
    upperMenu->addWidget(lITT);
    upperMenu->addWidget(tITT);
    upperMenu->addWidget(lPacketNr);
    upperMenu->addWidget(tPacketNr);
    upperMenu->addWidget(lPacketSz);
    upperMenu->addWidget(tPacketSz);
    upperMenu->addWidget(lTestDuration);
    upperMenu->addWidget(tTestDuration);*/
    upperMenu->addWidget(bStart);
    upperMenu->addWidget(bEnd);

    QLabel *lQtype1 = new QLabel("1st router queue type:");
    QLabel *lBWlim1 = new QLabel("1st router BW limit:");
    QLabel *lBuflim1 = new QLabel("1st router buffer limit:");

    QLabel *lQtype2 = new QLabel("2nd router queue type:");
    QLabel *lBWlim2 = new QLabel("2nd router BW limit:");
    QLabel *lBuflim2 = new QLabel("2nd router buffer limit:");


    tFirstRouterQtype = new QComboBox(this);
    tFirstRouterBWLimit = new QComboBox(this);
    tFirstRouterBufLimit = new QComboBox(this);
    tSecondRouterQtype = new QComboBox(this);
    tSecondRouterBWLimit = new QComboBox(this);
    tSecondRouterBufLimit = new QComboBox(this);

    qtypes.push_back("pfifo");
    qtypes.push_back("codel");
    qtypes.push_back("red");
    qtypes.push_back("pie");

  //  buflimits.push_back("100");
  //  buflimits.push_back("200");
  //  buflimits.push_back("500");
    buflimits.push_back("1000");

    bwlimits1.push_back("20Mbit");
    bwlimits1.push_back("50Mbit");
    bwlimits1.push_back("100Mbit");
    bwlimits1.push_back("1Gbit");

    bwlimits2.push_back("1Mbit");
    bwlimits2.push_back("5Mbit");
    bwlimits2.push_back("10Mbit");
    bwlimits2.push_back("15Mbit");


    for (auto it = qtypes.begin(); it != qtypes.end(); ++it) {
        tFirstRouterQtype->addItem(QString(it->c_str()));
        tSecondRouterQtype->addItem(QString(it->c_str()));
    }
    for (auto it = bwlimits1.begin(); it != bwlimits1.end(); ++it)
        tFirstRouterBWLimit->addItem(QString(it->c_str()));
    for (auto it = bwlimits2.begin(); it != bwlimits2.end(); ++it)
        tSecondRouterBWLimit->addItem(QString(it->c_str()));
    for (auto it = buflimits.begin(); it != buflimits.end(); ++it)
        tSecondRouterBufLimit->addItem(QString(it->c_str()));
    tFirstRouterBufLimit->addItem("1000");

    cNrStreams = new QComboBox(this);
    cNrStreams->addItem("0");
    cNrStreams->addItem("1");
    cNrStreams->addItem("2");
    cNrStreams->addItem("3");

    QLabel *lNrStreams = new QLabel("Background traffic: number of greedy flows:");

    upperMenu1->addWidget(lQtype1);
    upperMenu1->addWidget(tFirstRouterQtype);
    upperMenu1->addWidget(lBWlim1);
    upperMenu1->addWidget(tFirstRouterBWLimit);
    upperMenu1->addWidget(lBuflim1);
    upperMenu1->addWidget(tFirstRouterBufLimit);

    upperMenu2->addWidget(lQtype2);
    upperMenu2->addWidget(tSecondRouterQtype);
    upperMenu2->addWidget(lBWlim2);
    upperMenu2->addWidget(tSecondRouterBWLimit);
    upperMenu2->addWidget(lBuflim2);
    upperMenu2->addWidget(tSecondRouterBufLimit);

    upperMenu3->addWidget(lNrStreams);
    upperMenu3->addWidget(cNrStreams);

    plotQD = new QwtPlot(this);
    scatterQD = new QwtPlotSpectroCurve("QD");
    QwtLinearColorMap *colormap = new QwtLinearColorMap();
    scatterQD->setColorRange(QwtInterval(0,1));
    colormap->setColorInterval(QColor("blue"), QColor("red"));
    scatterQD->setColorMap(colormap);
    scatterQD->attach(plotQD);
    plotQD->setAxisTitle(0, "Queue delay (blue), losses (red)");
    plotQD->setAxisTitle(2, "Transmission time");
    plotQD->setAxisScale(2, 0, 5, 1);

    plotLD = new QwtPlot(this);
    scatterLD = new QwtPlotSpectroCurve("LD");
    scatterLD->attach(plotLD);
    plotLD->setAxisTitle(0, "Loss density");
    plotLD->setAxisTitle(2, "Transmission time");
    plotLD->setAxisScale(0, 0, 100, 20);
    plotLD->setAxisScale(2, 0, 5, 1);
    plotLD->setAxisAutoScale(2);

    scatterQD->setPenWidth(4);
    scatterLD->setPenWidth(4);

    plots->addWidget(plotQD);
    plots->addWidget(plotLD);

    tlQtype = new QLineEdit(this);
    tlQtype->setText("Queue type");
    tlQtype->setEnabled(false);

    QLabel *lQtype = new QLabel("Detected queue type:", this);
    detectedQtype->addWidget(lQtype);
    detectedQtype->addWidget(tlQtype);

    tlTestStatus = new QLineEdit(this);
    tlTestStatus->setText("not running");
    tlTestStatus->setEnabled(false);
    QLabel *lTestStatus = new QLabel("Test status:", this);
    status->addWidget(lTestStatus);
    status->addWidget(tlTestStatus);

    pTestProgress = new QProgressBar(this);
    pTestProgress->setValue(0);
    QLabel *lTestProgress = new QLabel("Test progress [%]:", this);
    progress->addWidget(lTestProgress);
    progress->addWidget(pTestProgress);

    connect(this, SIGNAL(scheduleReplot()), this, SLOT(commitData()));
    connect(bStart, SIGNAL(pressed()), this, SLOT(startTest()));
    connect(bEnd, SIGNAL(pressed()), this, SLOT(stopTest()));

    connect(tFirstRouterQtype, SIGNAL(currentIndexChanged(int)), this, SLOT(updateQType1(int)));
    connect(tSecondRouterQtype, SIGNAL(currentIndexChanged(int)), this, SLOT(updateQType2(int)));
    connect(tFirstRouterBWLimit, SIGNAL(currentIndexChanged(int)), this, SLOT(updateBWLim1(int)));
    connect(tSecondRouterBWLimit, SIGNAL(currentIndexChanged(int)), this, SLOT(updateBWLim2(int)));
    connect(tSecondRouterBufLimit, SIGNAL(currentIndexChanged(int)), this, SLOT(updateBufLim2(int)));

    bwlim2=bwlimits2.at(0);
    bwlim1=bwlimits1.at(0);
    qtype1=qtypes.at(0);
    qtype2=qtypes.at(0);
    buflim2=buflimits.at(0);
    buflim1="1000";
    nrstreams=0;

}

MainWindow::~MainWindow()
{
}

void MainWindow::updatePlot()
{
    plotDataQD.clear();
    plotDataLD.clear();
    std::cout << "output received" << std::endl;
    std::cout << "nr data points: " << res.numberOfDataPoints << std::endl;
    for (int i = 0; i < res.numberOfDataPoints; ++i) {
        QwtPoint3D pqd((double)res.pckQuDelay[i][0], (double)res.pckQuDelay[i][1]*1000, 0.0);
        QwtPoint3D pql((double)res.pckLossDensity[i][0], (double)res.pckLossDensity[i][1], 0);
        plotDataQD.append(pqd);
        plotDataLD.append(pql);
    }
    for (int i = 0; i < (res.nrpackets - res.numberOfDataPoints); ++i) {
        QwtPoint3D pl((double)res.pckLossSendTime[i], 0.0, 1.0);
        plotDataQD.append(pl);

    }
    qtype_out = res.queueType;
    test_status = res.message;
    scheduleReplot();
}


void MainWindow::prepareNewTest()
{
    QMutexLocker m(&paramMutex);
    t = new Test(this, &res, cport, itt, pnr, psz, tdur, qtype1, bwlim1, buflim1, qtype2, bwlim2, buflim2, nrstreams);
    testThread = new QThread;
    connect(testThread, SIGNAL(started()), t, SLOT(start()));
    connect(testThread, SIGNAL(finished()), this, SLOT(testCompleted()));

    t->moveToThread(testThread);

}

void MainWindow::startTest()
{
    prepareNewTest();
    testThread->start();
}
void MainWindow::stopTest()
{
    std::cout << "stopping test" << std::endl;

    prepareNewTest();
    QMutexLocker m(&dataMutex);

    bEnd->setEnabled(false);
    bStart->setEnabled(true);

}
void MainWindow::testCompleted()
{
    std::cout << "test completed" << std::endl;
    res = t->getOutput();
    delete t;
    t = 0;
    delete testThread;
    testThread = 0;
    updatePlot();
    scheduleReplot();
}

void MainWindow::commitData()
{
    QMutexLocker m(&dataMutex);
    scatterQD->setSamples(plotDataQD);
    scatterLD->setSamples(plotDataLD);
    plotQD->replot();
    plotLD->replot();
    tlQtype->setText(QString(qtype_out.c_str()));
    tlTestStatus->setText(QString(test_status.c_str()));

}

void MainWindow::updateQType1(int index)
{
    QMutexLocker m(&paramMutex);
    qtype1 = qtypes.at(index);
 /*   if (qtype1.compare(0, 3, "pie") == 0) {
        tFirstRouterBufLimit->setItemData(1,false, Qt::UserRole - 1);
    }*/

}

void MainWindow::updateQType2(int index)
{
    QMutexLocker m(&paramMutex);
    qtype2 = qtypes.at(index);
/*    if (qtype2.compare(0, 3, "pie") == 0) {
        tSecondRouterBufLimit->setItemData(2,0, Qt::UserRole - 1); //disable
    } else
        tSecondRouterBufLimit->setItemData(2,33, Qt::UserRole - 1); //enable
*/
    std::cout << "updated item data" << std::endl;
    scheduleReplot();
}

void MainWindow::updateBWLim1(int index)
{
     QMutexLocker m(&paramMutex);
     bwlim1 = bwlimits1.at(index);
}

void MainWindow::updateBWLim2(int index)
{
     QMutexLocker m(&paramMutex);
     bwlim2 = bwlimits2.at(index);
}

void MainWindow::updateBufLim1(int index)
{
     QMutexLocker m(&paramMutex);
     buflim1 = "1000";

}

void MainWindow::updateBufLim2(int index)
{
     QMutexLocker m(&paramMutex);
     buflim2 = buflimits.at(index);
}
void MainWindow::updateNrStreams(int index)
{
     QMutexLocker m(&paramMutex);
     nrstreams = index;
}
void MainWindow::updateIntermediateResult()
{
    std::cout << "received intermediate result" << std::endl;
    updatePlot();
    scheduleReplot();
}
