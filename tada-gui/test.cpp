#include "test.h"
#include "waitsignalthread.h"

#include <iostream>
#include <sstream>

Test::Test(MainWindow* m
           , result *res
           , int cport
           , int itt
           , int pnr
           , int psz
           , int tdur
           , std::string qtype1
           , std::string bwlim1
           , std::string buflim1
           , std::string qtype2
           , std::string bwlim2
           , std::string buflim2
           , int nrstreams)
    : mv(m)
    , res_out(res)
    , m_cport(cport)
    , m_itt(itt)
    , m_pnr(pnr)
    , m_psz(psz)
    , m_tdur(tdur)
    , m_qtype1(qtype1)
    , m_bwlim1(bwlim1)
    , m_buflim1(buflim1)
    , m_qtype2(qtype2)
    , m_bwlim2(bwlim2)
    , m_buflim2(buflim2)
    , m_nrstreams(nrstreams)
{
    memset(&cmd_args, 0, sizeof(struct cmd_args));
    memset(&cmd_args.udpConn, 0, sizeof(struct connectionInfo));
    memset(&cmd_args.controlConn, 0, sizeof(struct connectionInfo));
    memset(&cmd_args.glblPars, 0, sizeof(struct globalParameters));
    memset(&cmd_args.gui_output, 0, sizeof(struct result));
    cmd_args.glblPars.isReceiver = 1;
    cmd_args.glblPars.session_complete_mutex = PTHREAD_MUTEX_INITIALIZER;
    cmd_args.glblPars.unexpected_exit_mutex = PTHREAD_MUTEX_INITIALIZER;
    cmd_args.glblPars.udpthread_exit_mutex = PTHREAD_MUTEX_INITIALIZER;
    cmd_args.glblPars.udpthread_exit_cond = PTHREAD_COND_INITIALIZER;
    cmd_args.glblPars.signal_set_mutex = PTHREAD_MUTEX_INITIALIZER;

    cmd_args.glblPars.unexpected_exit = 0;
    cmd_args.glblPars.udpthread_exit = 1;
    cmd_args.glblPars.receiving = 1;

    cmd_args.udpConn.server_port = 12346;
    cmd_args.controlConn.server_port = 8181;
    strcpy(cmd_args.glblPars.resultFolderName, "./result");
    strcpy(cmd_args.glblPars.fileNamePrefix, "empty");

    mkpath(cmd_args.glblPars.resultFolderName);

    pthread_mutexattr_t errorcheck;
    pthread_mutexattr_init(&errorcheck);
    pthread_mutex_init(&cmd_args.gui_output.resmutex, &errorcheck);
    pthread_cond_init(&cmd_args.gui_output.resready, NULL);

}
void Test::configureRouters()
{
    std::cout << "tc commands..." << std::endl;
    //first router
    system("ssh router0 'tc qdisc del dev eth3 root'");
    system("ssh router0 'tc qdisc add dev eth3 root handle 1: htb default 10'");
    std::stringstream router1_rate;
    router1_rate << "ssh router0 'tc class add dev eth3 parent 1: classid 1:10 htb rate " << m_bwlim1 << " ceil " << m_bwlim1 << " burst 1516'";
    system(router1_rate.str().c_str());
    std::stringstream router1_qdisc;
    std::stringstream qdisc_param1;
    if (m_qtype1.compare(0,3,"red") == 0) {
        qdisc_param1 << " limit 1514000 avpkt 1514";
    } else
        qdisc_param1 << " limit " << m_buflim1;
    router1_qdisc << "ssh router0 'tc qdisc add dev eth3 parent 1:10 " << m_qtype1 << qdisc_param1.str() << "'";
    system(router1_qdisc.str().c_str());

    // second router
    std::cout << "second router" << std::endl;
    system("ssh router1 'tc qdisc del dev eth1 root'");
    system("ssh router1 'tc qdisc add dev eth1 root handle 1: htb default 10'");
    std::stringstream router2_rate;
    router2_rate << "ssh router1 'tc class add dev eth1 parent 1: classid 1:10 htb rate " <<  m_bwlim2 << " ceil " << m_bwlim2 << "'";
    std::cout << "setting rate: "<< router2_rate.str() << std::endl;
    std::cout << "bwlimit2: "<< m_bwlim2 << std::endl;

    system(router2_rate.str().c_str());
    std::stringstream router2_qdisc;
    std::stringstream qdisc_param2;
    if (m_qtype2.compare(0,3,"red") == 0) {
        qdisc_param2 << " limit 1514000 avpkt 1514";
    } else
        qdisc_param2 << " limit " << m_buflim2;
    router2_qdisc << "ssh router1 'tc qdisc add dev eth1 parent 1:10 " << m_qtype2 << qdisc_param2.str() << "'";
    system(router2_qdisc.str().c_str());

    std::cout << "DONE" << std::endl;

}

void Test::startBackgroundTraffic()
{
    // kill traffic that is running first
    if (m_nrstreams > 0){
        // commmands to start;
        std::stringstream command;
        command << "ssh xsender './streamzero_client -s 10.0.1.10 -p 10000 -P 15002 -I i:0,S:1448 -c " << m_nrstreams << "'";
        system(command.str().c_str());

        // Command for receiver machine
        // ./streamzero_srv -p 10000
    }

}

void Test::startReceiver()
{
        std::cout << "starting receiver" << std::endl;
        int status = 0;
        char filename[80];
        char logFileName[500];

         /*Opening Log file for Receiving*/
         strcpy(filename, "logData_rcv");

         create_resultfile_name(filename, &cmd_args, logFileName, 1);

        if ((cmd_args.glblPars.logfile_rcv = fopen(filename, "w")) <= 0){
                printf("   Could not Create Log File Output file!!!\n");
                status = -1;
        }

        std::cout << "starting sender.. " << std::endl;

        system("ssh sender './snd -s 10.0.1.10 -u 12346 -d 5 -a &' &");
        std::cout << "sender started" << std::endl;

        std::cout << "init control conn..." << std::endl;
        initializeControlConn_rcv(&cmd_args);
        std::cout << "DONE" << std::endl;

        //Initialize UDP server
        std::cout << "init UDP conn..." << std::endl;

        initializeUDPconn_rcv(&cmd_args);
        std::cout << "DONE" << std::endl;

        std::cout << "startRcvSideControlConn..." << std::endl;
        startRcvSideControlConn(&cmd_args);
        //std::cout << "nr of data points: " << cmd_args.gui_output.numberOfDataPoints << std::endl;
        std::cout << "DONE" << std::endl;

        pthread_mutex_lock(&cmd_args.glblPars.udpthread_exit_mutex);
            while (cmd_args.glblPars.udpthread_exit == 0)
                pthread_cond_wait(&cmd_args.glblPars.udpthread_exit_cond, &cmd_args.glblPars.udpthread_exit_mutex);
        pthread_mutex_unlock(&cmd_args.glblPars.udpthread_exit_mutex);

        if (cmd_args.updownOption == 1) {
            cmd_args.glblPars.receiving = 0;
            cmd_args.glblPars.sending = 1;
            printf("\n   Starting DownStream Test!!\n\n");

            status = resetRCV(&cmd_args);

            if (status > 0)
                startSndSideControlCon(&cmd_args);
        }
        close_program(&cmd_args);
        fclose(cmd_args.glblPars.logfile_rcv);

        switch (cmd_args.glblPars.currQueueType) {
            case QUEUETYPE_UNKNOWN:
                strcpy(cmd_args.finalQueueType, "Unknown");
                break;

            case QUEUETYPE_TD:
                strcpy(cmd_args.finalQueueType, "Tail-Drop");
                break;

            case QUEUETYPE_AQM:
                strcpy(cmd_args.finalQueueType, "AQM - Not able to detect the type");
                break;

            case QUEUETYPE_ARED:
                strcpy(cmd_args.finalQueueType, "ARED");
                break;

            case QUEUETYPE_CODEL:
                strcpy(cmd_args.finalQueueType, "CoDel");
                break;

            case QUEUETYPE_PIE:
                strcpy(cmd_args.finalQueueType, "PIE");
                break;
        }

        std::cout << "Final Queue Type Detected: " << cmd_args.finalQueueType << std::endl;

        std::cout << "returted from close_program" << std::endl;

}

struct result Test::getOutput()
{
    return cmd_args.gui_output;
}

void Test::start()
{

     WaitSignalThread *waitRes = new WaitSignalThread(&cmd_args.gui_output.resmutex, &cmd_args.gui_output.resready, &cmd_args.gui_output.testdone, this);
     QThread *waitThread = new QThread;
     connect(waitThread, SIGNAL(started()), waitRes, SLOT(start()));
     connect(waitRes, SIGNAL(newResult()), this, SLOT(intermediateResult()));
     connect(waitThread, SIGNAL(finished()), waitRes, SLOT(deleteLater()));
     waitRes->moveToThread(waitThread);
     waitThread->start();

     configureRouters();
     startBackgroundTraffic();
     startReceiver();

     std::cout << "exiting thread" << std::endl;
     QThread *mythread = this->thread();
     mythread->exit();


}
void Test::stop()
{

}

void Test::intermediateResult()
{
    std::cout << "received intermediate result" << std::endl;
    pthread_mutex_lock(&cmd_args.gui_output.resmutex);
    *res_out = cmd_args.gui_output;
    std::cout << "intermediate result: nr data points: " <<  cmd_args.gui_output.numberOfDataPoints << std::endl;
        pthread_mutex_unlock(&cmd_args.gui_output.resmutex);
    mv->updateIntermediateResult();
}
