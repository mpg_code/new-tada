#ifndef WAITSIGNALTHREAD_H
#define WAITSIGNALTHREAD_H

#include "test.h"
#include <QObject>
#include <pthread.h>

class WaitSignalThread: public QObject
{
    Q_OBJECT
public:
    explicit WaitSignalThread(pthread_mutex_t* lock, pthread_cond_t *cond, bool* testdone, Test *t);
signals:
    void newResult();
public slots:
    void start();

private:
    pthread_mutex_t* m_lock;
    pthread_cond_t* m_cond;
    bool* m_done;
    Test* m_tobj;

};

#endif // WAITSIGNALTHREAD_H
