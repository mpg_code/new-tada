#include "tada.h"
#include "analyzeResults.h"
#include "commonFunc.h"
#include "gnuplot_i.h"

void act_pct_pdt(double *data, int start, int end, double *pct, double *pdt) {
	int i;
	*pct = 0.0;
	*pdt = 0.0;

	double sum_pct = 0.0, sum_pdt = 0.0;

	for (i = start+1; i < end; i++) {
		if ( data[i] > data[i-1] ) 
      			sum_pct += 1;
		
    	sum_pdt += fabs(data[i] - data[i-1]);
  	}

	*pct = sum_pct / (end - start - 1);
	*pdt = (data[end-1] - data[start]) / sum_pdt;

}


void med_pct_pdt(double *data, int start, int end, double *pct, double *pdt, int inc) {
	int size = end - start;
	int sortArraySize = ((int)sqrt(size)) + 1;
	int medArraySize = sortArraySize;
	double *sort_array =  (double*) calloc(sizeof(double), sortArraySize);
	double *med_array = (double*) calloc(sizeof(double), medArraySize);
	int array_index = 0, limit = 0;
	int medArrayIndex = 0, sortArrayIndex = 0;
	int i = 0;
	double sum_pct = 0.0, sum_pdt = 0.0, sum_med = 0.0;

	array_index = start;
	
	while (array_index < end) {
		sortArrayIndex = 0;
		limit = array_index + sortArraySize;
		//printf("From %d to %d\n", array_index,limit);
		if (limit > end)
			limit = end;
		for (;array_index < limit;array_index++) {
			sort_array[sortArrayIndex] = data[array_index];
			sortArrayIndex++;
		}
		sort(sortArrayIndex,sort_array);
		med_array[medArrayIndex] = sort_array[sortArrayIndex/2];
		medArrayIndex++;
	}


	for (i = 1; i < medArrayIndex; i++) {
		if (inc == 1) {
			if ( med_array[i] > med_array[i-1] ) {
      			sum_pct += 1;
    		}			
		}
		else {
			if ( med_array[i] < med_array[i-1] ) {
      			sum_pct += 1;
    		}
		}

    	sum_pdt += fabs(med_array[i] - med_array[i-1]);
  	}

	*pct = sum_pct / (medArrayIndex - 1);
	*pdt = (med_array[medArrayIndex-1] - med_array[0]) / sum_pdt;


	free(sort_array);
	free(med_array);
}

double corr(int start, int end, double *x, double *y) {

	int i = 0;
	double xSum = 0.0, ySum = 0.0;
	double x2Sum = 0.0, y2Sum = 0.0, xySum = 0.0; 
	int size = end - start;

	//if (size < 100)
		//return -2;

	for (i = start; i < end; i++) {
		xSum += x[i];
		ySum += y[i];
		x2Sum += (x[i] * x[i]);
		y2Sum += (y[i] * y[i]);
		xySum += (x[i] * y[i]);
	}

	long double correlation = 0.0;
	double first = (size * xySum) - (xSum * ySum);
	double second = sqrt((size * x2Sum) - (xSum * xSum)) * sqrt((size * y2Sum) - (ySum * ySum));
	if (second != 0)
		correlation = first / second;
	else
		correlation = 0.0;

	return correlation;

}

double corrWithX(int start, int end, double *x) {

	int i = 0;
	double index = 0.0;
	double xSum = 0.0, ySum = 0.0;
	double x2Sum = 0.0, y2Sum = 0.0, xySum = 0.0; 
	int size = end - start;

	for (i = start; i < end; i++) {
		xSum += x[i];
		ySum += index;
		x2Sum += (x[i] * x[i]);
		y2Sum += (index * index);
		xySum += (x[i] * index);

		index += 1.0;
	}


	long double correlation = 0.0;
	double first = (size * xySum) - (xSum * ySum);
	double second = sqrt((size * x2Sum) - (xSum * xSum)) * sqrt((size * y2Sum) - (ySum * ySum));
	correlation = first / second;

	return correlation;

}

int find_maximum_index(int size, double *data){
	int i = 1;
	int index = 0;
	double maximum = data[0];
	for (; i < size; i++) {
		if (data[i] > maximum) {
			maximum = data[i];
			index = i;
		}
	}
	return index;
}

double calculate_send_rate(struct streamInfo *stream, FILE *logfile) {
	int firstRcvPckNr = stream->firstRcvSeqNr;
	int lastRcvPckNr = stream->lastRcvSeqNr;
	int numberOfReceivedPck = stream->pckCountReceived;
	int packetSize = stream->rcvPckSize_byte;
	int i = 0;
	double rate = 0.0;

	double receiveDuration = 0.0;
	
	if (numberOfReceivedPck > 0) {
		receiveDuration = stream->packet[lastRcvPckNr].receiveTime - stream->packet[firstRcvPckNr].receiveTime;

		rate = (double)numberOfReceivedPck * packetSize / receiveDuration;
		fprintf(logfile, "\n      Calculated Rate is %f\n", rate);	
	}
	

	return rate;
}

int calculate_loss_density(struct cmd_args *cmd_args, double *pckLossDensity, double *sm_pckLossDensity, int limit, int maxQtSeqNr) {

	struct streamInfo *stream = &cmd_args->udpConn.strInfo;
	struct globalParameters *glblPars = &cmd_args->glblPars; 

    int i = 0,j = 0;
    int seqNrCounter = 0;
    double res = 0;
    double sm_lossDensity, lossDensity;
    int numberOfSentPck = stream->rcvPckCount;
    FILE *output_file;



	char filename[80];
	strcpy(filename, RCV_LOSSDENSITY_FILENAME);
	create_resultfile_name(filename, cmd_args, glblPars->lossDensityFileName, 0);

    if ((output_file = fopen(glblPars->lossDensityFileName, "w")) <= 0){
    	fprintf(glblPars->logfile_rcv, "   Could not create output file for Loss Density\n");
    	return -1;
    }

    while (i < numberOfSentPck) {
    	lossDensity = 0.0;
    	sm_lossDensity = 0.0;
    	for (j = 0; j < numberOfSentPck; j++) {
    		if (stream->packet[j].payloadSize == 0) {
    			res = -1.0 * ((double)((i - j) * (i - j)) / 2000);
    			lossDensity += ( (double)1.2616 * ((double)exp(res)));
    			if (j < maxQtSeqNr) {
    				if (j == 0) {
    					sm_lossDensity += ( (double)1.2616 * ((double)exp(res)));
    				}
    				else if (stream->packet[j-1].payloadSize != 0 ||
    					stream->packet[j+1].payloadSize != 0) {
    					sm_lossDensity += ( (double)1.2616 * ((double)exp(res)));
    				}
    			}
    			else
    				sm_lossDensity += ( (double)1.2616 * ((double)exp(res)));
    			
    		}
    	}
    	if (stream->packet[i].payloadSize != 0) {
    		pckLossDensity[seqNrCounter] = lossDensity;
    		sm_pckLossDensity[seqNrCounter] = sm_lossDensity;
    		seqNrCounter++;
    		fprintf(output_file,"%lf\t",stream->packet[i].rel_sendTime);
    		fprintf(output_file,"%e\n",lossDensity);
    	}
    	i++;
    	
    }

    fclose(output_file);
    return 1;

}


int analyzeResults(struct cmd_args *cmd_args, double lossRate) {

	/* Local Variables for Stream Parameters*/
	fprintf(cmd_args->glblPars.logfile_rcv, "\n   Results:\n");
	fprintf(cmd_args->glblPars.logfile_rcv, "\n      lossRate: %d\%\n", (int)lossRate);
	printf("\n      lossRate: %d\%\n", (int)lossRate);

	struct streamInfo *stream = &cmd_args->udpConn.strInfo;

	int numberOfReceivedPck = stream->pckCountReceived;
	int numberOfSentPck = stream->rcvPckCount;
	int numberOfLostPck = numberOfSentPck - numberOfReceivedPck;
	int packetSize = stream->rcvPckSize_byte;
	int interTrnsmTime = stream->rcvPckITT_micros;
	int transmission_duration = stream->transmission_duration;
	int firstRcvPckNr = stream->firstRcvSeqNr;
	int fastestPckSeqNr = stream->fastestPckSeqNr;
	int firstLostPckSeqNr = -1;

	double calculated_rate = 0.0;
	int i = 0;
	char filename[80];

	FILE *queueTime_output_file, *lossNumber_output_file;
	double max_qTime = 0.0;
	int max_qTimeSeqNr = -1;

	double avg_itt = (double) transmission_duration / numberOfSentPck / 1000000.0; // approximate ITT
	
	/*Opening Queue Time Result file*/
	strcpy(filename, RCV_QUEUETIME_FILENAME);
	create_resultfile_name(filename, cmd_args, cmd_args->glblPars.queueTimeFileName, 0);

    if ((queueTime_output_file = fopen(cmd_args->glblPars.queueTimeFileName, "w")) <= 0){
    	fprintf(cmd_args->glblPars.logfile_rcv, "   Could not create output file for Packets' Queueing Time: %s\n",cmd_args->glblPars.queueTimeFileName);
    	return -1.0;
    }

    /* Opening Lost Packet Seq# file */
	strcpy(filename, RCV_LOSSNUMBER_FILENAME);
	create_resultfile_name(filename, cmd_args, cmd_args->glblPars.lossNumberFileName, 0);

    if ((lossNumber_output_file = fopen(cmd_args->glblPars.lossNumberFileName, "w")) <= 0){
    	fprintf(cmd_args->glblPars.logfile_rcv, "   Could not create output file for Loss Packet Number\n");
    	return -1.0;
    }
	/* Open Output file for QueueDelay and LossNumber */

	if (cmd_args->glblPars.tada_phase == RATE_DETECTION_PHASE) {
		calculated_rate = calculate_send_rate(&cmd_args->udpConn.strInfo, cmd_args->glblPars.logfile_rcv);
		calculated_rate = ceil(calculated_rate);
		int rate_to_int = (int) calculated_rate;
		

		/* While loop Just For QueueDelay and LossNumber and maybe LossDensity */
		for (i = 0; i < numberOfSentPck; i++) {
			stream->packet[i].rel_sendTime = (double) i * avg_itt;
			if (stream->packet[i].payloadSize != 0) {
				stream->packet[i].queueingDelay = (double) fabs(stream->packet[i].oneWayDelay - stream->min_OWD);
				stream->packet[i].rel_receiveTime = stream->packet[i].receiveTime - stream->packet[firstRcvPckNr].receiveTime;
				//printf("Packet Number %d qdelay is %f\n", i, owd);
				fprintf(queueTime_output_file, "%d\t", i);
    			fprintf(queueTime_output_file, "%lf\t", stream->packet[i].rel_sendTime);
    			fprintf(queueTime_output_file, "%lf\t", stream->packet[i].receiveTime);
    			fprintf(queueTime_output_file, "%e\n", stream->packet[i].queueingDelay);

    			if (stream->packet[i].queueingDelay > max_qTime) {
    				max_qTime = stream->packet[i].queueingDelay;
    				max_qTimeSeqNr = i;
    			}
			}
			else {
				if (firstLostPckSeqNr == -1)
    				firstLostPckSeqNr = i;
    			fprintf(lossNumber_output_file, "%d\t", i);
    			fprintf(lossNumber_output_file, "%lf\t", stream->packet[i].rel_sendTime);
    			fprintf(lossNumber_output_file, "%lf\t", stream->packet[i].sendTime);
    			if ( i < firstRcvPckNr)
    				fprintf(lossNumber_output_file, "%e\n", stream->packet[firstRcvPckNr].queueingDelay);
    			else
    				fprintf(lossNumber_output_file, "%e\n", stream->packet[i-1].queueingDelay);
			}
		}
		


		fclose(queueTime_output_file);
    	fclose(lossNumber_output_file);
		return rate_to_int;
	} // End of if (cmd_args->tada_phase == RATE_DETECTION_PHASE)
	else if (cmd_args->glblPars.tada_phase == AQM_VS_TD_PHASE) {

		//memset(&cmd_args->gui_output, 0, sizeof(struct result));
		/* Parameters */
		int sortArray_size = (int)(sqrt(numberOfReceivedPck)/2);
		int medArray_size = ((int)numberOfReceivedPck / sortArray_size) + 1;
		int medArray_index = 0, sortArray_index = 0;
		int mainArray_index = 0, limit = 0;
		double sum_pct = 0.0, sum_pdt = 0.0;
		double pct = 0.0, pdt = 0.0;
		int increasing_index = 0;
		int nonincreasing_index = 0;
		int pct_index = 0;
		int pdt_index = 0;
		int decreasing_index = 0;
		int increasing_number = 0;
		int mainIncNr = 0;
		int lastIncPckSeqNr = 0;
		int max_qTimeIndex = 0;
		int pckLossIndex = 0;
		int queueType = QUEUETYPE_UNKNOWN;

		/* Allocating Space for sortArray and medArray */
		double *sortArray =  (double*) calloc(sizeof(double), sortArray_size);
		double *medArray = (double*) calloc(sizeof(double), medArray_size);
		double *minArray = (double*) calloc(sizeof(double), medArray_size);

		/* Allocating Space for pckQuDelay and pckSeqNr */
		double *pckQuDelay = (double*) calloc(sizeof(double), numberOfReceivedPck);
		int *pckSeqNr = (int *) calloc(sizeof(int), numberOfReceivedPck);
		double *pckSendTime = (double*) calloc(sizeof(double), numberOfReceivedPck);
		double *pckLossSendTime = (double*) calloc(sizeof(double), numberOfLostPck);
		double *pckLossDensity = (double*) calloc(sizeof(double), numberOfReceivedPck);
		double *sm_pckLossDensity = (double*) calloc(sizeof(double), numberOfReceivedPck);

		/* While loop */
		/* Calculate QueueDelay and Find Increase Part and Max Queue Delay */

		for (i = 0; i < numberOfSentPck; i++) {
			stream->packet[i].rel_sendTime = (double) i * avg_itt;
			if (stream->packet[i].payloadSize != 0) {
				stream->packet[i].queueingDelay = (double) fabs(stream->packet[i].oneWayDelay - stream->min_OWD);
				stream->packet[i].rel_receiveTime = stream->packet[i].receiveTime - stream->packet[firstRcvPckNr].receiveTime;
				//printf("Packet Number %d qdelay is %f\n", i, owd);
				fprintf(queueTime_output_file, "%d\t", i);
    			fprintf(queueTime_output_file, "%lf\t", stream->packet[i].rel_sendTime);
    			fprintf(queueTime_output_file, "%lf\t", stream->packet[i].rel_receiveTime);
    			fprintf(queueTime_output_file, "%e\n", stream->packet[i].queueingDelay);

    			if (stream->packet[i].queueingDelay > max_qTime) {
    				max_qTime = stream->packet[i].queueingDelay;
    				max_qTimeSeqNr = i;
    				max_qTimeIndex = mainArray_index;
    			}

    			/* Find Median Queue Delay for Current sortArray  */
    			sortArray[sortArray_index] = stream->packet[i].queueingDelay;
    			sortArray_index++;
    			pckQuDelay[mainArray_index] = stream->packet[i].queueingDelay;
    			pckSeqNr[mainArray_index] = i;
    			pckSendTime[mainArray_index] = stream->packet[i].rel_sendTime;
    			mainArray_index++;
    			if (sortArray_index == sortArray_size || mainArray_index >=  numberOfReceivedPck) {
    				sort(sortArray_index,sortArray);
    				medArray[medArray_index] = sortArray[sortArray_index/2];
    				int n = 0;

    				while (sortArray[n] == 0.0)
    					n++;
    				minArray[medArray_index] = sortArray[n];
    				//printf("minArray[%d] is %lf\n", medArray_index, minArray[medArray_index]);
    				medArray_index++;
    				sortArray_index = 0;
    				memset(sortArray, 0.0, sizeof(sortArray));

    			}

			}
			else {
				if (firstLostPckSeqNr == -1)
    				firstLostPckSeqNr = i;
    			fprintf(lossNumber_output_file, "%d\t", i);
    			fprintf(lossNumber_output_file, "%lf\t", stream->packet[i].rel_sendTime);
    			fprintf(lossNumber_output_file, "%lf\t", stream->packet[i].rel_receiveTime);
    			if ( i < firstRcvPckNr)
    				fprintf(lossNumber_output_file, "%e\n", stream->packet[firstRcvPckNr].queueingDelay);
    			else
    				fprintf(lossNumber_output_file, "%e\n", stream->packet[i-1].queueingDelay);
    			pckLossSendTime[pckLossIndex] = stream->packet[i].rel_sendTime;
    			pckLossIndex++;
			}
		} // End of for (i = 0; i < numberOfSentPck; i++)
		int sw = -1;

		int fastestPckGroupIndex = fastestPckSeqNr / sortArray_size;

		fprintf(cmd_args->glblPars.logfile_rcv, "   Fastest Packet Seq Number: %d\n", fastestPckSeqNr);

		double firstCorr = 0.0;
		int isFirstIncreasing = 0;

		sw = 0;

		/* Find the Increasing Part Index */
		for (i = fastestPckGroupIndex + 1; i < medArray_index; i++) {
			if ( medArray[i] > medArray[i-1] )
      			sum_pct += 1;
      		sum_pdt += fabs(medArray[i] - medArray[i-1]);

      		pct = sum_pct / (i - fastestPckGroupIndex);

      		pdt = (medArray[i] - medArray[fastestPckGroupIndex]) / sum_pdt;

      		if ((pct > 0.55 && pdt > 0.55)){
				increasing_index = i;
				sw = 1;
      		}
      		if (pct > 0.55)
      			pct_index = i;

      		if (pdt > 0.55)
      			pdt_index = i;

      		if (pct < 0.54 && pdt > (-1 * 0.45) && pdt < 0.45)
      			nonincreasing_index = i;
		}

		fprintf(cmd_args->glblPars.logfile_rcv, "   pdt_index: %d, pct_index: %d, increasing_index: %d\n", 
			pdt_index, pct_index, increasing_index);

		if (sw == 1) { // Found an Increasing Part
			

			int fip = increasing_index * sortArray_size;
			pct = 0.0;
			pdt = 0.0;

			med_pct_pdt(pckQuDelay, fastestPckSeqNr, fip, &pct, &pdt, 1);

			fprintf(cmd_args->glblPars.logfile_rcv, "   PCT and PDT for FIP: %lf, %lf\n", pct, pdt);

			if (pct > 0.5 && pdt > 0.4) {
				isFirstIncreasing = 1;
				pct = 0.0;
				pdt = 0.0;
				firstCorr = 0.0;
			}

			if (isFirstIncreasing == 1) {
				increasing_number = (increasing_index + 1) * sortArray_size;
			}
			else {
				increasing_number = 0;
				if (pct < 0.54 && pdt > (-1 * 0.45) && pdt < 0.5)
					queueType = QUEUETYPE_TD;
				else if (pct < 0.54 && pdt > 0.55)  {
					increasing_number = (increasing_index + 1) * sortArray_size;
				}
			}
		}
	
		 
		if (increasing_number > numberOfReceivedPck)
			increasing_number = numberOfReceivedPck;

		mainIncNr = increasing_number;

		


		double ratio = ((double) increasing_number / numberOfReceivedPck);
		
		//printf("   Increasing Number: %d\n", increasing_number);
		
		if ((ratio > 0.025)) {

			if (increasing_number != numberOfReceivedPck)
				increasing_number = (increasing_index + 2) * sortArray_size;

			if (increasing_number > numberOfReceivedPck)
				increasing_number = numberOfReceivedPck;

			lastIncPckSeqNr = pckSeqNr[increasing_number - 1];

			calculate_loss_density(cmd_args, pckLossDensity, sm_pckLossDensity, 
					increasing_number, max_qTimeSeqNr);
			/* Find out if MaxQ is in the range */
			if (max_qTimeSeqNr > lastIncPckSeqNr) {
				max_qTimeIndex = find_maximum_index(increasing_number, pckQuDelay);
				max_qTimeSeqNr = pckSeqNr[max_qTimeIndex];
			}			
			if (firstLostPckSeqNr == -1) {
				if (pckQuDelay[max_qTimeSeqNr] > 0.15) {
					queueType = QUEUETYPE_TD;
					cmd_args->glblPars.isDefintlyTD = 1;
				
				}
			}
			else if (lossRate > 15.0) {

				queueType = QUEUETYPE_AQM;

				int end_index = 0;
				int corrSize = 0;
				int limit = increasing_number * 0.95;

				if (ratio > 0.95 && max_qTimeIndex > limit)
					end_index = max_qTimeIndex * 0.95;
				else
					end_index = max_qTimeIndex;

				double total_pct = 0.0, total_pdt = 0.0;
				int isLeftPartIncreasing = 0, isRightPartIncreasing = 0;
				int isTotalIncreasing = 0;
				double total_corr = 0.0;
				double leftPartCorr = 0.0; // Left Part 
				double rightPartCorr = 0.0;

				/* Divide the whole priod to two section and calculate the correlation */

				double leftPartPct = 0.0, leftPartPdt = 0.0;
				double rightPartPct = 0.0, rightPartPdt = 0.0;
				int sw = 0; // A switch set to 1 when one of the parts has low correlation

				int middleIndex = (end_index - firstLostPckSeqNr) * 0.5 + firstLostPckSeqNr;
				//printf("   MaxQtimeIndex: %d\n", max_qTimeIndex);
				//printf("   First Loss SeqNr: %d\n", firstLostPckSeqNr);
				//printf("   Middle Index: %d\n", middleIndex);
				//printf("   MaxQtimeIndex * 0.75: %d\n", end_index);
				//printf("   Loss Density Total Range CORR %lf\n", total_corr);

				double lossDpct = 0.0, lossDpdt = 0.0;
				double tempCorr = 0.0;

				corrSize = end_index - firstLostPckSeqNr;

				total_corr = corr(firstLostPckSeqNr, end_index, pckSendTime, sm_pckLossDensity);
				leftPartCorr = corr(firstLostPckSeqNr, middleIndex, pckSendTime, sm_pckLossDensity);
				rightPartCorr = corr(middleIndex, end_index, pckSendTime, sm_pckLossDensity);
				act_pct_pdt(sm_pckLossDensity, firstLostPckSeqNr, end_index, &lossDpct, &lossDpdt);

				fprintf(cmd_args->glblPars.logfile_rcv, "   TotalCorr is: %lf\n", total_corr);
				fprintf(cmd_args->glblPars.logfile_rcv, "   rightPartCorr is: %lf\n", rightPartCorr);
				fprintf(cmd_args->glblPars.logfile_rcv, "   leftPartCorr is: %lf\n", leftPartCorr);
				fprintf(cmd_args->glblPars.logfile_rcv, "   LossD PCT is %lf\n", lossDpct);
				fprintf(cmd_args->glblPars.logfile_rcv, "   LossD PDT is %lf\n", lossDpdt);

				//printf("   TotalCorr is: %lf\n", total_corr);
				//printf("   rightPartCorr is: %lf\n", rightPartCorr);
				//printf("   leftPartCorr is: %lf\n", leftPartCorr);
				//printf("   LossD PCT is %lf\n", lossDpct);
				//printf("   LossD PDT is %lf\n", lossDpdt);
				
				if (total_corr <= DEFAULT_CORR_THRESHOLD) {
					queueType = QUEUETYPE_TD;
				}
				else {
					int start = firstLostPckSeqNr, end = start + 3 * sortArray_size;
					double slope = 0.0, angle = 0.0;
					double qD_rangeCorr = 0.0;
					double val = 180.0 / PI;
					do {
						
						slope = 0.0;
						angle = 0.0;

						//slope_calculator(pckSeqNr, pckQuDelay, start, end, 
							//minArray[0], &slope);
						qD_rangeCorr = corr(start, end_index, pckSendTime, pckQuDelay);

						//angle = (double)atan(slope) * val;
						//fabs(angle) <= 15.00
						if (qD_rangeCorr <= DEFAULT_CORR_THRESHOLD) {
							double sm_rangeCorr = corr(start, end_index, pckSendTime, sm_pckLossDensity);
							if (sm_rangeCorr <= DEFAULT_CORR_THRESHOLD) {
								double m_rangeCorr = corr(start, end_index, pckSendTime, pckLossDensity);
								if (m_rangeCorr <= DEFAULT_CORR_THRESHOLD) {
									fprintf(cmd_args->glblPars.logfile_rcv, "   rangeCorr from %d to %d is %lf\n", 
										start, end_index, m_rangeCorr);
									//printf("   rangeCorr from %d to %d is %lf\n", 
										//start, end_index, m_rangeCorr);

									queueType = QUEUETYPE_TD;
									break;
								}
								 
							}
						}
							
						start += 1;
						end += 1;

					} while (end <= end_index);

				}
				
				int maxQtGroupIndex = max_qTimeIndex / sortArray_size;

				double R_Squared = 0.0;

				cmd_args->glblPars.rSquaredCount++;

				R_Squared = total_corr * total_corr;

				cmd_args->glblPars.currRsquared += R_Squared;
										
				if (queueType != QUEUETYPE_TD &&
					(medArray_index - maxQtGroupIndex + 1) >= 10) {

					queueType = QUEUETYPE_ARED;
					
					sum_pct = 0.0, sum_pdt = 0.0;
					pct = 0.0, pdt = 0.0;
					sw = 0;
					decreasing_index = 0;
					for (i = maxQtGroupIndex + 1; i < medArray_index; i++) {
						if ( medArray[i] < medArray[i-1] )
      						sum_pct += 1;
      					sum_pdt += fabs(medArray[i] - medArray[i-1]);

      					pct = sum_pct / (i - maxQtGroupIndex - 1);

      					pdt = (medArray[i] - medArray[maxQtGroupIndex+1]) / sum_pdt;

      					if ((pct > 0.55 && pdt < (-1 * 0.55))){
							decreasing_index = i;
							sw = 1;
      					}
					}

					double decRatio = 0.0;
					double incRatio = 0.0;

					if (sw == 1) {
						int decNum = (decreasing_index + 1) * sortArray_size;
						if (decNum > numberOfReceivedPck)
							decNum = numberOfReceivedPck;
						double max = 0.0;
						double min = 0.0;

						decRatio = ((double) (decreasing_index - maxQtGroupIndex) * sortArray_size / numberOfReceivedPck);
						incRatio = (double)max_qTimeIndex / numberOfReceivedPck;

						if (decreasing_index == medArray_index -1)
							queueType = QUEUETYPE_PIE;
						else {
							int rangeMinQtIndex = 0;

							double decPct = 0.0, decPdt = 0.0;
							
							rangeMinQtIndex = find_min(pckQuDelay, max_qTimeIndex, decNum, &min);

							act_pct_pdt(sm_pckLossDensity, max_qTimeIndex, mainIncNr, &decPct, &decPdt);
							fprintf(cmd_args->glblPars.logfile_rcv, "   decPct is %lf, decPdt is %lf\n", decPct, decPdt);
							if (min == 0.0)
								queueType = QUEUETYPE_PIE;
							else {
								double maxTomin = (double) pckQuDelay[max_qTimeSeqNr] / min;
								if (maxTomin >= 4.0)
									queueType = QUEUETYPE_PIE;
								else if (decPdt > 0.55 && decPct > 0.55)
									queueType = QUEUETYPE_PIE;
							} 
								
						}
							
					}


					if (queueType == QUEUETYPE_PIE){
						int arrayEnd = max_qTimeIndex;
						arrayEnd = arrayEnd * 0.85;
						queueType == QUEUETYPE_PIE;
							
						double avgRsquared = 0.0;

						avgRsquared = cmd_args->glblPars.currRsquared / (double)cmd_args->glblPars.rSquaredCount;							

						fprintf(cmd_args->glblPars.logfile_rcv, "   avg R-squared is %lf\n", avgRsquared);

						//avgRsquared *= avgRsquared;

						if (avgRsquared >= 0.9) {
							queueType = QUEUETYPE_CODEL;
						}
								
						
					}
				}

				/* Preparing for Generating the Plots*/
				char plotFileName[500], plotTitle[500];
				strcpy(plotFileName, cmd_args->glblPars.queueTimeFileName);
				strcat(plotFileName, ".eps");

				if (strcmp(cmd_args->glblPars.fileNamePrefix, "empty") != 0)
					sprintf(plotTitle, "%s Queueing Delay: %dpackets, %dmicrosITT, %dbyte", cmd_args->glblPars.fileNamePrefix, 
						numberOfSentPck, interTrnsmTime, packetSize);
				else
					sprintf(plotTitle, "Queueing Delay: %dpackets, %dmicrosITT, %dbyte",  
						numberOfSentPck, interTrnsmTime, packetSize);
				//TODO: make gnuplot figures: 
				queueTime_lossDensity_plot(plotFileName, cmd_args->glblPars.queueTimeFileName, cmd_args->glblPars.lossNumberFileName, 
					cmd_args->glblPars.lossDensityFileName, transmission_duration, pckQuDelay[max_qTimeIndex], 
					pckSendTime[firstLostPckSeqNr], pckSendTime[max_qTimeIndex]);
			} 

		}


		int end_index = 0;
		if (numberOfReceivedPck <= MAX_RECEIVED_PACKET) {
			end_index = numberOfReceivedPck;
			cmd_args->gui_output.numberOfDataPoints = numberOfReceivedPck;					 
		}
		else {
			end_index = MAX_RECEIVED_PACKET;
			cmd_args->gui_output.numberOfDataPoints = MAX_RECEIVED_PACKET;					
		}

		for(i = 0; i < end_index; i++) {
			cmd_args->gui_output.pckQuDelay[i][1] = pckQuDelay[i];
			cmd_args->gui_output.pckQuDelay[i][0] = pckSendTime[i];
			cmd_args->gui_output.pckLossDensity[i][1] = pckLossDensity[i];
			cmd_args->gui_output.sm_pckLossDensity[i][1] = sm_pckLossDensity[i];
			cmd_args->gui_output.pckLossDensity[i][0] = pckSendTime[i];
			cmd_args->gui_output.sm_pckLossDensity[i][0] = pckSendTime[i];
		}

		if (numberOfLostPck <= MAX_RECEIVED_PACKET) {
			end_index = numberOfLostPck;
		}
		else {
			end_index = MAX_RECEIVED_PACKET;
		}

		for(i = 0; i < end_index; i++) {
			cmd_args->gui_output.pckLossSendTime[i] = pckLossSendTime[i];
		}

		cmd_args->gui_output.testnr = cmd_args->glblPars.testNumber;
		cmd_args->gui_output.nrpackets = numberOfSentPck;
		cmd_args->gui_output.duration_sec = transmission_duration;
		cmd_args->gui_output.packetsz = packetSize;
		cmd_args->gui_output.ITT_usec = interTrnsmTime;
		cmd_args->gui_output.lossrate = lossRate;
		cmd_args->gui_output.numberOfLossDDataPoints = increasing_number;
		int rate = (int)(packetSize * 1000000 / cmd_args->gui_output.ITT_usec);
		int nextRate = 0;
		cmd_args->gui_output.rate = (int)(rate / 1024.0);
		nextRate = (int)(rate + rate * DEFAULT_RATEADJUSTMENT_FACTOR);
		cmd_args->gui_output.nextSendRate = (int)(nextRate / 1024.0);
		sprintf(cmd_args->gui_output.message, "Sending 5 seconds Stream with Rate %dKBps", cmd_args->gui_output.nextSendRate);

		switch (queueType) {
			case QUEUETYPE_UNKNOWN:
				strcpy(cmd_args->gui_output.queueType, "Unknown");
				break;

			case QUEUETYPE_TD:
				strcpy(cmd_args->gui_output.queueType, "Tail-Drop");
				break;

			case QUEUETYPE_AQM:
				strcpy(cmd_args->gui_output.queueType, "AQM - Not able to detect the type");
				break;

			case QUEUETYPE_ARED:
				strcpy(cmd_args->gui_output.queueType, "ARED");
				break;

			case QUEUETYPE_CODEL:
				strcpy(cmd_args->gui_output.queueType, "CoDel");
				break;

			case QUEUETYPE_PIE:
				strcpy(cmd_args->gui_output.queueType, "PIE");
				break;
		}


		free(sortArray);
		free(medArray);
		free(minArray);
		free(pckQuDelay);
		free(pckSendTime);
		free(pckSeqNr);
		free(pckLossSendTime);
		free(pckLossDensity);
		free(sm_pckLossDensity);
		fclose(queueTime_output_file);
    	fclose(lossNumber_output_file);


    	pthread_mutex_lock(&cmd_args->gui_output.resmutex);
    		pthread_cond_broadcast(&cmd_args->gui_output.resready);
    	pthread_mutex_unlock(&cmd_args->gui_output.resmutex);

    	return queueType;

	} // End of if (cmd_args->tada_phase == AQM_VS_TD_PHASE)
	
	return 0;

	
	/* Close Output file for QueueDelay and LossNumber */

}