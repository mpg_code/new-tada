#include <stdbool.h>
#include <sys/ioctl.h>

void act_pct_pdt(double *data, int start, int end, double *pct, double *pdt);
void med_pct_pdt(double *data, int start, int end, double *pct, double *pdt, int inc);
double corr(int start, int end, double *x, double *y);
double corrWithX(int start, int end, double *x);
int find_maximum_index(int size, double *data);
double calculate_send_rate(struct streamInfo *stream, FILE *logfile);
int calculate_loss_density(struct streamInfo *stream, struct globalParameters *glblPars, 
	double *pckLossDensity, double *sm_pckLossDensity, int limit, int maxQtSeqNr);
int analyzeResults(struct cmd_args *cmd_args, double lossRate);