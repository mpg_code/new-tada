
#ifndef _TADA_H
#define _TADA_H

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <getopt.h>

#include <sys/types.h>
#include <utime.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <time.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <errno.h>
#include <fcntl.h>
#include <assert.h>
#include <pthread.h>
#include <stdlib.h>
#include <net/if.h>
#include <stdarg.h>
#include <sys/poll.h>
#include <float.h>
#include <limits.h>
#include <stdbool.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>


#ifdef __linux__
#include <sys/epoll.h>
#include <sys/ioctl.h>
#elif __APPLE__
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <sys/filio.h>
#endif


#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"
#define RESET "\033[0m"
#define KBOLD "\x1B[1m"

#define PI 3.14159265358979323


#define RATE_DETECTION_PHASE 0
#define AQM_VS_TD_PHASE 1
#define PACKETB_BYTEB_PHASE 2
#define REPETITION_PHASE 3
#define FINISH_PHASE 4
#define FIRST_SESSION 5

#define GETINITIALMESSAGE_STAGE 0
#define GETFINISHMESSAGE_STAGE 1

#define QUEUETYPE_UNKNOWN 0
#define QUEUETYPE_TD 1
#define QUEUETYPE_AQM 2
#define QUEUETYPE_ARED 3
#define QUEUETYPE_CODEL 4
#define QUEUETYPE_PIE 5
#define QUEUETYPE_PB_TD 6
#define QUEUETYPE_BB_TD 7


#define MIN_WAIT_TIME 2

#define ERROR -1;

/* data buffer size for socket operations */

#define DEFAULT_RESULT_FOLDER "./results"
#define DEFAULT_FILE_PREFIX "empty"
#define DEFAULT_CONTROLCH_PORT 8181
#define UDPSTREAM_START_PORT 16002
#define CONTROLCH_START_PORT 17002

#define MAX_PACKETSIZE_DEFAULT 1472
#define MIN_PACKETSIZE_DEFAULT 200
#define DEFAULT_TEST_DURATION 10 //second
#define DEFAULT_PACKET_COUNT 7000 //second
#define DEFAULT_START_ITT 100 //second

#define MAX_RECEIVED_PACKET 50000

#define DEFAULT_CORR_THRESHOLD 0.4
#define DEFAULT_RATEADJUSTMENT_FACTOR 0.05

#define DEFAULT_STOP_LOSSRATE 30

#define TRUE             1
#define FALSE            0

/* Max connection requests */
#define MAXPENDING 50

/* Max connections at server */
#define MAXSRCVCONN 60000

#define EPOLL_TIMEOUT 10000

#define RCV_QUEUETIME_FILENAME "queueTime"
#define RCV_LOSSNUMBER_FILENAME "lossNumber"
#define RCV_LOSSDENSITY_FILENAME "lossDensity"
#define RCV_DERIVATION_FILENAME "qTime_derivation"
#define qType_FILENAME "qType"
#define RCV_INFO_FILENAME "info"

#define RATEDETECTION_PACKET_COUNT 4000

#define TTABLE_ROW_NUMBER 67
#define TTABLE_COLUMN_NUMBER 8
#define DEFAULT_GROUP_SIZE 30 /* Group Size for t-test implementation*/

struct globalParameters
{
	float lastLossRate;

	//extern int phase;
	int currITT;
	int sendRate;
	int currQueueType;
	int currLossRate;
	double currRsquared;
	double finalRsquared;
	int rSquaredCount;
	int isFirstTest;

	int tada_phase;
	int test_stage;

	int tdDetectedCount;
	int aqmDetectedCount;
	int aredDetectedCount;
	int pieDetectedCount;
	int codelDetectedCount;

	int isDefintlyTD;
	int isReceiver;

	int sessionNr;


	char fileNamePrefix[1500];       /* prefix of result files names  */
	char resultFolderName[1500];
	char queueTimeFileName[1500];  
	char infoFileName[1500];    
	char lossDensityFileName[1500];      
	char lossNumberFileName[1500];      
	char queueDerivationFileName[1500];
	char pctPdtMedianFileName[1500];
	char queueTypeFileName[1500];

	FILE *logfile_rcv;
	FILE *logfile_snd;
	
	int testNumber;

	time_t test_time; 
	pthread_mutex_t session_complete_mutex;
	int session_complete;

	pthread_mutex_t unexpected_exit_mutex;
	int unexpected_exit;

	pthread_mutex_t signal_set_mutex;
	int signal_set;

	pthread_mutex_t udpthread_exit_mutex;
	pthread_cond_t udpthread_exit_cond;
	int udpthread_exit;
	int client_shutdown;
	char currentTime[80];

	int udp_packetCount;
	int udp_packetSize;

	int sending; // 1 of currently sending, else 0
	int receiving; // 1 of currently receiving, else 0
	int channelError;
	int *channelErrorArray;
	int channelErrorArraySize;

};

struct result
{
	char queueType[80];
	double pckQuDelay[MAX_RECEIVED_PACKET][2]; 
	double pckLossDensity[MAX_RECEIVED_PACKET][2];
	double sm_pckLossDensity[MAX_RECEIVED_PACKET][2];
	double pckLossSendTime[MAX_RECEIVED_PACKET];
	int numberOfDataPoints;
	pthread_mutex_t resmutex;
	pthread_cond_t resready;
	int testnr;
	int rate;
	int nrpackets;
	int duration_sec;
	int packetsz;
	int ITT_usec;
	int lossrate;
	bool testdone;
	int nextSendRate;
	char message[1000];
	int numberOfLossDDataPoints;

};


struct streamInfo {
	
	// Client-Side Variables	
	int sentPckCount;
	int sentPckITT_micros;
	int sentPckSize_byte;
	int lastSentSeqNr;
	struct timeval session_startT;
	struct timeval session_endT;
	int bytes_sent;
	int packets_sent;

	// Server-Side Variables
	int bytes_received;
	int pckCountReceived;
	int rcvPckCount;
	int rcvPckITT_micros;
	int rcvPckSize_byte;
	int firstRcvSeqNr;
	int lastRcvSeqNr;
	int firstLostSeqNr;
	int foundFirstLostSeqNr;
	int fastestPckSeqNr;	
	double min_OWD;
	struct packetInfo *packet;

	int transmission_duration;
};

struct connectionInfo {
	int protocol;
	struct streamInfo strInfo;
	FILE *dev_null;
	struct sockaddr_in si_me, si_other;
	

	// Receiver-Side Parameters
	int listensock;
	int received;
	int client_port;
	int start_port;
	struct timeval start_time;
	// UDP socket
	int sock;
	

	// Sender-Side Parameters
	struct hostent* host;
	int server_port;


};

struct cmd_args {
    char *ifname;       /* interface name (such as "en0") */

	struct connectionInfo udpConn;
	struct connectionInfo controlConn;

	int is_automatic_test;
	int updownOption;
	int interfaceOption;
	int pckReceiveTime;
	int quickack;
	char chennelErrorFile[80];
	int sndSideTransmission_duration;


	struct globalParameters glblPars;

	char *program_name;
	int help;

	struct result gui_output;
	char finalQueueType[80];


};

struct packetInfo
{
  double sendTime;        /* sent timestamp for packet */
  double prSendTime;      /* sent timestamp for previous packet*/
  double receiveTime;     /* absolute receive timestamp for packet */
  double rel_receiveTime;
  double rel_sendTime;
  double queueingDelay;
  double oneWayDelay;
  int payloadSize;                /* payload size */
  uint32_t seqNr;
};

#endif
