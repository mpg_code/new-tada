#include <stdbool.h>
#include <sys/ioctl.h>

#define TIME_WAIT 1000

int initializeControlConn_rcv(struct cmd_args *cmd_args);
int initializeUDPconn_rcv(struct cmd_args *cmd_args);
int resetRCV(struct cmd_args *cmd_args);
void signal_handler(int sig);