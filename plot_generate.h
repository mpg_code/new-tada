#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "gnuplot_i.h"
#include "tada.h"
#define DEFAULT_QUEUETIME_XLABEL "Packet Nr"
#define DEFAULT_QUEUETIME_YLABEL "Queueing Delay[ms]"
#define DEFAULT_QUEUETIME_COLOR "blue"
#define DEFAULT_LOSSNUMBER_COLOR "red"
#define DEFAULT_QUEUETIME_TITLE "Queueing Delay For Delivered Packets"
#define DEFAULT_LOSSNUMBER_TITLE "Lost Packets"

int queueTime_lossDensity_plot(char *plotFileName, char *queueTime, 
	char *lossNr, char *lossDensity, int test_duration, 
	double max_qTime, double frstLssNr_Time, double maxPckNr_Time);

int plot(char *plotFileName, char *plotTitle, char *fileName,
	char *xLabel, char *yLabel, char *coordinate, char *color);