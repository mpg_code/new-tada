all: rcv snd

libtada_rcv: tada_rcv.o rcv_parse_opts.o rcv_controlCH.o rcv_udpStream.o snd_controlCH.o snd_udpStream.o gnuplot_i.o analyzeResults.o commonFunc.o
	ar cr libtada_rcv.a tada_rcv.o rcv_parse_opts.o rcv_controlCH.o rcv_udpStream.o snd_controlCH.o snd_udpStream.o gnuplot_i.o analyzeResults.o commonFunc.o

rcv: main_rcv.c tada.h libtada_rcv
	g++ -Wall main_rcv.c -std=c++0x -L. -ltada_rcv -o rcv -lm -lpthread

tada_rcv.o: tada_rcv.c
	g++ -c -g tada_rcv.c

rcv_udpStream.o: rcv_udpStream.c rcv_udpStream.h
	g++ -c -g rcv_udpStream.c

analyzeResults.o: analyzeResults.c analyzeResults.h
	g++ -c -g analyzeResults.c

rcv_controlCH.o: rcv_controlCH.c rcv_controlCH.h
	g++ -c -g rcv_controlCH.c

gnuplot_i.o: gnuplot_i.c gnuplot_i.h
	g++ -c -g gnuplot_i.c

rcv_parse_opts.o: rcv_parse_opts.c rcv_controlCH.h
	g++ -c -g rcv_parse_opts.c

libtada_snd: tada_snd.o snd_parse_opts.o snd_controlCH.o snd_udpStream.o rcv_controlCH.o rcv_udpStream.o gnuplot_i.o analyzeResults.o commonFunc.o
	ar cr libtada_snd.a tada_snd.o snd_parse_opts.o snd_controlCH.o snd_udpStream.o rcv_controlCH.o rcv_udpStream.o gnuplot_i.o analyzeResults.o commonFunc.o

snd: main_snd.c tada.h libtada_snd
	g++ -Wall main_snd.c -std=c++0x -L. -ltada_snd -o snd -lm -lpthread

tada_snd.o: tada_snd.c tada.h
	g++ -c -g tada_snd.c

snd_parse_opts.o: snd_parse_opts.c snd_parse_opts.h
	g++ -c -g snd_parse_opts.c

snd_udpStream.o: snd_udpStream.c snd_udpStream.h
	g++ -c -g snd_udpStream.c

snd_controlCH.o: snd_controlCH.c snd_controlCH.h
	g++ -c -g snd_controlCH.c

commonFunc.o: commonFunc.c commonFunc.h
	g++ -c -g commonFunc.c
	
clean:
	rm -f *.o
	rm -f tada_snd
	rm -f tada_rcv
	rm -f libtada_rcv.a
	rm -f libtada_snd.a
