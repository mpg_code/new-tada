void exitUdpRcvThread(struct cmd_args *cmd_args);
int updateUdpStream(struct streamInfo *stream, int packetCount, int packetSize, int packetITT);
int initializeUdpStream(struct streamInfo *stream);
int insertNewPck(struct streamInfo *stream, char *buffer, int buffer_size);
void *handle_udpThread(void *arg);
int startUdpRcv(struct cmd_args *cmd_args);
int handle_new_udpPck(struct connectionInfo *conn);
int read_udpBytes(int sock, char *buf, int bytes_to_read, int *bytes_read, struct sockaddr_in *remote_addr, socklen_t *addr_size);
