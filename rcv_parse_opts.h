#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <libgen.h>

#define OPTSTRING "u:c:I:P:o:i:p:m:d:Trah"

void usage(char* argv);
void parse_cmd_args_rcv(int argc, char *argv[], struct cmd_args *cmd_args);
void required_argument_check(char *optarg, struct cmd_args *cmd_args, char arg, char *argv);