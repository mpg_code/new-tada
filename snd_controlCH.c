#include "tada.h"
#include "snd_controlCH.h"
#include "commonFunc.h"
#include "snd_udpStream.h"

int sendInitialMessages(int sock, struct connectionInfo *udpStream_ci) {
	int pNr = udpStream_ci->strInfo.sentPckCount;
	int itt = udpStream_ci->strInfo.sentPckITT_micros;
	int pSize = udpStream_ci->strInfo.sentPckSize_byte;
	
	pNr = htonl(pNr);
	itt = htonl(itt);
	pSize = htonl(pSize);
	/*Number of Packets*/
	if (send(sock, &pNr, sizeof(pNr), 0) <= 0 ) {
        perror("   send the number of sent packets failed!!!");
        return ERROR;
    }

    /*Packets' ITT*/
	if (send(sock, &itt, sizeof(itt), 0) <= 0 ) {
        perror("   send the Inter-transmission Time Failed!!!");
        return ERROR;
    }

    /*Size of Packets*/
    if (send(sock, &pSize, sizeof(pSize), 0) <= 0 ) {
     	perror("   send the Packet Size Failed!!!");
    	return ERROR;
    }

	return 1;

}


int receiveFinishMessages(int sock, int *phase, int *currQueueType, int *sendRate, float *lastLossRate,
	struct connectionInfo *connInfo) {

	int status = 1;
	if (*phase == PACKETB_BYTEB_PHASE) {
		status = receiveMessage(sock);
		*phase = FINISH_PHASE;
		if (status < 0)
			return ERROR;
		// Finish the test

	}
	else if (*phase == AQM_VS_TD_PHASE) {

		// Receive Loss Rate
		*lastLossRate = status = receiveMessage(sock);
		if (status < 0)
			return ERROR;

		if (status >= DEFAULT_STOP_LOSSRATE && *currQueueType != QUEUETYPE_TD)
			*phase = FINISH_PHASE;

		// Receive tail-drop AQM or unknown
		status = receiveMessage(sock);
		if (status < 0)
			return ERROR;
		
		// Change the Phase if it is tail-drop or we reach to the loss rate threshold
		if (status == QUEUETYPE_TD) {
			*phase = PACKETB_BYTEB_PHASE;
			*currQueueType = QUEUETYPE_TD;
		}

	}
	/* Rate Detection Phase is Finished */
	else if (*phase == RATE_DETECTION_PHASE) {
		// Receive Only Rate From the Receiver
		status = receiveMessage(sock);

		if (status > 1) {
			*sendRate = status;
			//printf("   The Bottleneck Bandwidth is %dBps\n", *sendRate);
			/* Calculate the ITT or PacketSize */	
		}
		else
			return ERROR;
		*phase = AQM_VS_TD_PHASE;
				 
	}

	return 1;
}

int startSndSideControlCon(struct cmd_args *cmd_args) {

	int status = 1;

	struct connectionInfo *controlCh_ci = &cmd_args->controlConn;
	struct connectionInfo *udpStream_ci = &cmd_args->udpConn;
	
	struct timeval test_duration;
	int transmission_duration = 0;

	while (status > 0) {
		status = getUdpStreamSpecifications(cmd_args);
		
		fprintf(cmd_args->glblPars.logfile_snd, "------------------------------------------------------\n");
		
		if (status == -1) {
			printf("\n   Less Than 512Kbps Rate, Not able to Detect!!!\n");
			return status;
		}
		status = sendInitialMessages(controlCh_ci->sock, udpStream_ci);
		if (status == 1) {
			status = receiveMessage(controlCh_ci->sock);
			
			if (status != -1 && status != -2) { // -1 means Error, -2 means shut down
				
				fprintf(cmd_args->glblPars.logfile_snd, "\n   Starting a Session for %d packets, %d ITT, %d packetSize\n", 
					udpStream_ci->strInfo.sentPckCount, udpStream_ci->strInfo.sentPckITT_micros, 
					udpStream_ci->strInfo.sentPckSize_byte);

				status = send_udpStream(cmd_args, &cmd_args->glblPars.currITT);			
				if (status != -1) {
					
					timersub(&udpStream_ci->strInfo.session_endT, &udpStream_ci->strInfo.session_startT, &test_duration);

					transmission_duration = (test_duration.tv_sec * 1000000 + test_duration.tv_usec);
					transmission_duration = ntohl(transmission_duration);

					if (send(controlCh_ci->sock, &transmission_duration, sizeof(transmission_duration), 0) <= 0 ) {
        				perror("   Send the Duration message to RCV failed!!!");
        				status = -1;
    				}
    				else
    					fprintf(cmd_args->glblPars.logfile_snd, "\n   The Session Is Finished, Transmission Time is Sent\n");

    				if (status != -1)
					{
						status = receiveFinishMessages(controlCh_ci->sock, &cmd_args->glblPars.tada_phase, &cmd_args->glblPars.currQueueType, 
							 &cmd_args->glblPars.sendRate, &cmd_args->glblPars.lastLossRate, udpStream_ci);
						
						if (status != -1) {
							fprintf(cmd_args->glblPars.logfile_snd, "\n   Qtype Message is Received!!!\n\n");
							status = 1;
						}
					}			
				}
			} 
			
		}			
		if (cmd_args->glblPars.tada_phase == FINISH_PHASE) {
			// If it is Client just break
			// Otherwise wait for Client to Shut Down
			if (cmd_args->glblPars.isReceiver == 0)
				break;
			else {
				receiveMessage(controlCh_ci->sock); 
				break;
			}
		}

		fprintf(cmd_args->glblPars.logfile_snd, "------------------------------------------------------\n\n");

	}

	return status;

}
