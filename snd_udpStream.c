#include "tada.h"
#include "snd_udpStream.h"
#include "commonFunc.h"

int udp_fd_valid(int fd) {
	if (fcntl(fd, F_GETFL) == -1 && errno == EBADF)
		return 0;
	return 1;
}

int getUdpStreamSpecifications(struct cmd_args *cmd_args) {

	struct connectionInfo *connInfo = &cmd_args->udpConn;
	int itt = 0;

	int localSendRate = cmd_args->glblPars.sendRate;
	int phase = cmd_args->glblPars.tada_phase; 
	int isFirstTest = cmd_args->glblPars.isFirstTest; 
	
	if (phase == PACKETB_BYTEB_PHASE) {

		connInfo->strInfo.sentPckITT_micros = (int)(connInfo->strInfo.sentPckITT_micros / 5);
		itt = connInfo->strInfo.sentPckITT_micros;
		connInfo->strInfo.sentPckSize_byte = (int)(itt * cmd_args->glblPars.sendRate / 1000000);
		if (connInfo->strInfo.sentPckSize_byte < MIN_PACKETSIZE_DEFAULT) {
			connInfo->strInfo.sentPckSize_byte = MIN_PACKETSIZE_DEFAULT;
			connInfo->strInfo.sentPckITT_micros = (int)(connInfo->strInfo.sentPckSize_byte * 1000000 / cmd_args->glblPars.sendRate);
		}
					 
	}
	else if (phase == AQM_VS_TD_PHASE) {
		if (cmd_args->glblPars.isFirstTest == 1) { // First test after rate detection step we increase detected rate by the factor 20%
			cmd_args->glblPars.sendRate+=(int)(cmd_args->glblPars.sendRate * 0.02);
			cmd_args->glblPars.isFirstTest = 0;
		}
		else {
			cmd_args->glblPars.sendRate+=(int)(cmd_args->glblPars.sendRate * DEFAULT_RATEADJUSTMENT_FACTOR);
		}


		itt = (int)(cmd_args->glblPars.udp_packetSize * 1000000 / cmd_args->glblPars.sendRate);
		fprintf(cmd_args->glblPars.logfile_snd, "   SendRate is %dBps and ITT is %dMicros\n", cmd_args->glblPars.sendRate, itt);
		if (itt >= 5000) {
			itt = 5000;
			connInfo->strInfo.sentPckSize_byte = (int)(itt * cmd_args->glblPars.sendRate / 1000000);
			if (connInfo->strInfo.sentPckSize_byte < MIN_PACKETSIZE_DEFAULT) {
				connInfo->strInfo.sentPckSize_byte = MIN_PACKETSIZE_DEFAULT;
				itt = (int)(connInfo->strInfo.sentPckSize_byte * 1000000 / cmd_args->glblPars.sendRate);
				if (itt > 5000)
					return ERROR;
			}

		}
		else
			connInfo->strInfo.sentPckSize_byte = cmd_args->glblPars.udp_packetSize;


		connInfo->strInfo.sentPckITT_micros = itt;
		


		if (connInfo->strInfo.transmission_duration != 0) {
			connInfo->strInfo.sentPckCount = (connInfo->strInfo.transmission_duration * 1000000) / connInfo->strInfo.sentPckITT_micros;
			fprintf(cmd_args->glblPars.logfile_snd, "The Numebr Of Packets to Send %d\n", connInfo->strInfo.sentPckCount);
		
		}
		else {
			connInfo->strInfo.sentPckCount = cmd_args->glblPars.udp_packetCount;
		}// Assign the numebr of packets and itt and packet size for this phase first
		// Assign the numebr of packets and itt and packet size for this phase first
	}
	else if (phase == RATE_DETECTION_PHASE) {
		cmd_args->glblPars.udp_packetCount = connInfo->strInfo.sentPckCount;
		cmd_args->glblPars.udp_packetSize = connInfo->strInfo.sentPckSize_byte;
		connInfo->strInfo.sentPckCount = RATEDETECTION_PACKET_COUNT;
		connInfo->strInfo.sentPckITT_micros = DEFAULT_START_ITT;
		connInfo->strInfo.sentPckSize_byte = MAX_PACKETSIZE_DEFAULT;
		cmd_args->glblPars.isFirstTest = 1;
	}

	cmd_args->glblPars.tada_phase = phase; 

	return 1;
}

int send_udpStream(struct cmd_args *cmd_args, int *currITT) {

	int res;
	
	int status = 1;

	struct connectionInfo *connInfo = &cmd_args->udpConn;
		
	connInfo->strInfo.bytes_sent = 0;
	connInfo->strInfo.packets_sent = 0;
	connInfo->strInfo.lastSentSeqNr = 0;

	*currITT = connInfo->strInfo.sentPckITT_micros;

	status = write_udp_data(cmd_args);

	
	if (connInfo->strInfo.bytes_sent == 0) {
		fprintf(cmd_args->glblPars.logfile_snd, "Haven't sent any data!\n");
	}


	deplete_sendbuffer(connInfo);

	return status;

}

int write_udp_data(struct cmd_args *cmd_args) {

	
	struct connectionInfo *ci = &cmd_args->udpConn;
	// parameters
	int send_size = 0;
	send_size = ci->strInfo.sentPckSize_byte;

	int sleep_time_ms;
	struct timeval endT, sendTime, itt, sendTime_t;
	itt.tv_sec = 0;
	itt.tv_usec = ci->strInfo.sentPckITT_micros;
	struct timeval startT, elapsedTime;
	
	time_t end_time = 0;
	int packet_count = 0;
	time_t now;

	packet_count = ci->strInfo.sentPckCount;

	/* Sending Start Time for this Test*/
	gettimeofday(&sendTime, NULL);

	double prPck_sTime = 0.0;
	double curPck_sTime = 0.0;
	

	memset(&ci->strInfo.session_startT, 0, sizeof(struct timeval));
	gettimeofday(&ci->strInfo.session_startT, NULL);


	while (1) {

		if (ci->strInfo.packets_sent == packet_count) {
			memset(&ci->strInfo.session_endT, 0, sizeof(struct timeval));
			gettimeofday(&ci->strInfo.session_endT, NULL);
			return 1;
		}
		

		// Ok, lets send a packet

		int rc;
		// Sending zeroes
		
		gettimeofday(&startT, NULL);

		rc = send_data(cmd_args, send_size, prPck_sTime, &curPck_sTime);
		
		if (rc == -1) { // -1 means Error
			perror("UDP sendto() failed!!!");
			return ERROR;

		}
		else { // -2 means Channel Error packet, more than zero means bytes sent
			ci->strInfo.bytes_sent += send_size;
			ci->strInfo.packets_sent += 1;
			ci->strInfo.lastSentSeqNr += 1;
			prPck_sTime = curPck_sTime;
			curPck_sTime = 0.0;
		}

		timeradd(&sendTime, &itt, &sendTime_t);
		sendTime = sendTime_t;

		gettimeofday(&endT, NULL);

		if (timercmp(&endT, &sendTime, <) > 0) {
			timersub(&sendTime, &endT, &elapsedTime);
			select(0, NULL, NULL, NULL, &elapsedTime);

		}
	}

}

int send_data(struct cmd_args *cmd_args, uint32_t size, 
	double prPck_sTime, double *sTime) {

	/* Initialize send buffer */
	char buf[MAX_PACKETSIZE_DEFAULT];
	memset(buf, 0, MAX_PACKETSIZE_DEFAULT);

	struct connectionInfo *ci = &cmd_args->udpConn;

	struct timeval now;

	if (size < MIN_PACKETSIZE_DEFAULT) {
		fprintf(stderr, "Size of data (%d) cannot be less than MIN_PACKETSIZE (%d)", size, MIN_PACKETSIZE_DEFAULT);
		return -1;
	}

	if (cmd_args->glblPars.channelError == 1 && cmd_args->glblPars.tada_phase != RATE_DETECTION_PHASE
		&& cmd_args->glblPars.isFirstTest == 0
		&& ci->strInfo.lastSentSeqNr < cmd_args->glblPars.channelErrorArraySize) {
		if (cmd_args->glblPars.channelErrorArray[ci->strInfo.lastSentSeqNr] == 1) {
			return -2; // Didn't send any data
		}
	}

	// seqNr is stored as string
	snprintf(&buf[0], 11, "%d", ci->strInfo.lastSentSeqNr);
	
	gettimeofday(&now, NULL);
	tv2s(&now, sTime);

	memcpy(&buf[11], sTime, sizeof(double));

	double *testNr = (double*)&buf[11];



	int sent;
	if (send_all(ci, buf, size, &sent, 0) < 0) {
		return ERROR;
	}

	return sent;

}

int send_all(struct connectionInfo *ci, char *buf, int len, int *sent, int flags) {

	int total = 0;        // how many bytes we've sent
	int bytesleft = len; // how many we have left to send
	int n;

	while (total < len) {
		n = sendto(ci->sock, buf + total, bytesleft, flags,
                      (struct sockaddr*)&ci->si_other, sizeof(ci->si_other));

		if (n == -1) {
			break;
		}
		total += n;
		bytesleft -= n;
	}
	*sent = total; // return number actually sent here

	return n == -1 ? -1 : 0; // return -1 on failure, 0 on success

}