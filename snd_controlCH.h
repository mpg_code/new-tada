#include <float.h>

int sendInitialMessages(int sock, struct connectionInfo *udpStream_ci);
int receiveFinishMessages(int sock, int *phase, int *currQueueType, int *sendRate, float *lastLossRate,
	struct connectionInfo *connInfo);
int startSndSideControlCon(struct cmd_args *cmd_args);