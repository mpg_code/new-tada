#include "plot_generate.h"


int queueTime_lossDensity_plot(char *plotFileName, char *queueTime, 
	char *lossNr, char *lossDensity, int test_duration, 
	double max_qTime, double frstLssNr_Time, double maxPckNr_Time)
{
	double round_max_qTime = ceil(max_qTime * 1000);
	int rqt = (int) round_max_qTime;
	int five_remain = rqt % 5;
	rqt += (5 - five_remain);
	int yticsNr = rqt / 5;

	double xrange = (double) test_duration / 1000000.0;
	gnuplot_ctrl * g = gnuplot_init();
	gnuplot_cmd(g, "set terminal postscript eps enhanced color \"Courier\" 20");
	gnuplot_cmd(g, "set output \"%s\"", plotFileName);
	gnuplot_cmd(g, "set nokey");
	gnuplot_cmd(g, "set size 2");
	gnuplot_cmd(g, "set grid xtics ytics");

	gnuplot_cmd(g, "set object 2 rectangle from %f,0 to %f,%d fillcolor rgb \"#FFE4C4\" behind",
		frstLssNr_Time, maxPckNr_Time, 3000);
	
	gnuplot_cmd(g, "set ytics %d nomirror", yticsNr);
	gnuplot_cmd(g, "set ylabel 'Queueing Delay[ms]'");
	gnuplot_cmd(g, "set y2tics 20 nomirror");
	gnuplot_cmd(g, "set y2label 'Loss Density'");
	gnuplot_cmd(g, "set xlabel 'Sent Time[s]'");
	//gnuplot_cmd(g, "set xrange [0:%f]", xrange);
	if (test_duration > 1000000)
		gnuplot_cmd(g, "set xtics 1");
	gnuplot_cmd(g, "set y2range [0:100]");
	gnuplot_cmd(g, "set origin 0,0");
	gnuplot_cmd(g, "set style line 1 lc rgb '#0060ad' lt 1 lw 5 pt 7 pi -1 ps 1.5");
	gnuplot_cmd(g, "plot \"%s\" using 2:($4 * 1000) with dots ls 1 lw 6 lc rgb \"blue\" title \"%s\",\\", 
		queueTime, DEFAULT_QUEUETIME_TITLE); 
	gnuplot_cmd(g, "\"%s\" using 2:3 with dots ls 1 lw 6 lc rgb \"black\" notitle axes x1y2, \\", 
		lossDensity); 
	gnuplot_cmd(g, "\"%s\" using 2:($4 * 0) with dots ls 1 lw 6 lc rgb \"%s\" title \"%s\"", lossNr, 
			DEFAULT_LOSSNUMBER_COLOR, DEFAULT_LOSSNUMBER_TITLE);
	//gnuplot_cmd(g, "unset object 1");
	gnuplot_close(g);

	return 0 ;
}

int plot(char *plotFileName, char *plotTitle, char *fileName,
	char *xLabel, char *yLabel, char *coordinate, char *color)
{
	gnuplot_ctrl * g = gnuplot_init();
	gnuplot_cmd(g, "set terminal postscript eps enhanced color \"Courier\" 20");
	gnuplot_cmd(g, "set output \"%s\"", plotFileName);
	//gnuplot_cmd(g, "set title \"%s\"", plotTitle);
	gnuplot_cmd(g, "set key below");
	gnuplot_cmd(g, "set grid xtics ytics");
	gnuplot_cmd(g, "set size 2");
	gnuplot_cmd(g, "set size ratio 0.7");
	gnuplot_set_xlabel(g, xLabel);
	gnuplot_set_ylabel(g, yLabel);
	gnuplot_cmd(g, "plot \"%s\" using %s with lines ls 1 lw 10 lc rgb \"%s\" title \"%s\"", fileName, 
		coordinate, color, plotTitle);
	gnuplot_close(g);

	return 0;
}