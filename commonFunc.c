#include "tada.h"
#include "commonFunc.h"
#include "gnuplot_i.h"

int find_min(double *data, int start, int end, double *min) {
	int i = 0;
	int index = 0;
	double lmin = 0.0; // Local max and Local min
	lmin = data[start];
	for (i = start; i < end; i++) {
		if (data[i] < lmin) {
			lmin = data[i];
			index = i;
		}
	}

	*min = lmin;
	
	return index;

}

int sort(int number, double *array) {
	
	int i = 0, j = 0;
	double swap = 0.0;

	for (i = 0 ; i < (number - 1); i++)
  	{
    	for (j = 0 ; j < (number - i - 1); j++)
    	{
      		if (array[j] > array[j+1]) /* For decreasing order use < */
      		{
        		swap = array[j];
        		array[j] = array[j+1];
        		array[j+1] = swap;
      		}
    	}
  	}

}

void slope_calculator(int *x, double *y, int start, int end, 
	double minY, double *slope) {
	double xySigma = 0.0;
	double x2Sigma = 0.0;
	*slope = 0.0;
	int i = 0;
	int j = 0;

	int size = end - start;

	double medY = 0.0;
	double medX = x[size / 2 + start];

	double *data = (double*) calloc(sizeof(double), size);


	for (i = start; i < end; i++) {
		data[j] = y[i];
		j++;
	}

	sort(size, data);

	medY = data[size / 2];	

	medY /= minY;


	for (i = start; i < end; i++) {
		xySigma += (x[i] - medX) * ((y[i] / minY) - medY);
		x2Sigma += (x[i] - medX) * (x[i] - medX);
	}

	*slope = xySigma / x2Sigma;


}

int set_error_signal(struct cmd_args *cmd_args) {
	pthread_mutex_lock(&cmd_args->glblPars.unexpected_exit_mutex);
		cmd_args->glblPars.unexpected_exit = 1;
	pthread_mutex_unlock(&cmd_args->glblPars.unexpected_exit_mutex);

	return -1;
}

void calculate_standardDeviation(double *data, int start, int size, double *sd, double *avg) {
	int i = 0;
	double sum_data = 0.0;
	double sum_diff = 0.0;
	double data_avg = 0.0;

	int end = start + size;

	for (i = start; i < end; i++) {
		sum_data += data[i];
	}
	////printf("Sum of Data is %f\n", sum_data);
	data_avg = (double)sum_data / size;

	for (i = start; i < end; i++) {
		sum_diff += ((double)(data[i] - data_avg) * (data[i] - data_avg));
	}
	////printf("SumDiff of Data is %f\n", sum_diff);
	*sd = (double)sqrt(sum_diff / (size - 1));
	*avg = data_avg;

}

void calculate_standardError(int n1, int n2, double s1, double s2, double *se) {
	double sp = 0.0;
	

	double nr1 = (double)(n1-1) * s1 * s1;
	double nr2 = (double)(n2-1) * s2 * s2;
	double df = n1+n2-2;
	double nr3 = (double)(n1+n2)/(n1 * n2);

	////printf("nr1 is %lf, nr2 is %lf, nr3 is %lf\n", nr1, nr2, nr3);

	sp = (double)sqrt((nr1 + nr2) / df);

	////printf("sp is %lf\n", sp);
	
	*se = (double)sp * (double)sqrt(nr3);

	////printf("Stadndard Error is %lf\n", *se);

}

void copyArray(double *array1, double *array2, int array2Size) {
	int i;

	for (i = 0; i < array2Size; i++) {
		array1[i] = array2[i];
		array2[i] = 0.0;
	}
}

int fd_set_blocking(int fd, int blocking) {
    /* Save the current flags */
    int flags = fcntl(fd, F_GETFL, 0);
    if (flags == -1)
        return 0;

    if (blocking)
        flags &= ~O_NONBLOCK;
    else
        flags |= O_NONBLOCK;
    return fcntl(fd, F_SETFL, flags) != -1;
}

void close_program(struct cmd_args *cmd_args) {
	struct connectionInfo *udpConn_ci = &cmd_args->udpConn;
	struct connectionInfo *controlConn_ci = &cmd_args->controlConn;

	if (udpConn_ci->sock != 0)
		close(udpConn_ci->sock);

	////printf("Closed udpConn_ci->sock\n");

	if (controlConn_ci->dev_null != NULL) {
		fclose(controlConn_ci->dev_null);
		controlConn_ci->dev_null = NULL;
	}

	if (controlConn_ci->listensock != 0)
		close(controlConn_ci->listensock);

	if (controlConn_ci->sock != 0)
		close(controlConn_ci->sock);


	
	////printf("Closed controlConn_ci->listensock\n");

}

void create_resultfile_name(char *filename, struct cmd_args *cmd_args, 
	char *resultFileName, int isQueueType) {

	float lossRate;
	int lr = 0;

	struct streamInfo *stream = &cmd_args->udpConn.strInfo;

	char *fileNamePrefix = cmd_args->glblPars.fileNamePrefix; 
	char *resultFolderName = cmd_args->glblPars.resultFolderName;

	int numberOfSentPck = stream->rcvPckCount;
	int numberOfLostPck = numberOfSentPck - stream->pckCountReceived;
	int packetSize = stream->rcvPckSize_byte;
	int interTrnsmTime = stream->rcvPckITT_micros;

	if (numberOfSentPck == 0)
		lossRate = 0;
	else
		lossRate = (numberOfLostPck * 100 / numberOfSentPck);

	lr = (int) lossRate;

	if (isQueueType == 1) {

		if (strcmp(fileNamePrefix, "empty") != 0) {
			sprintf(resultFileName, "%s/%s_%s_%s", resultFolderName, fileNamePrefix,
				filename, cmd_args->glblPars.currentTime);
		}
		else
			sprintf(resultFileName, "%s/%s_%s", resultFolderName,
				filename, cmd_args->glblPars.currentTime);
	}
	else if (isQueueType == 0) {
		if (strcmp(fileNamePrefix, "empty") != 0) {
			sprintf(resultFileName, "%s/%s_%dsp_%dMs_%dB_%dlr_%s_%s", resultFolderName, fileNamePrefix,
				numberOfSentPck, interTrnsmTime, packetSize, lr, filename, cmd_args->glblPars.currentTime);
		}
		else
			sprintf(resultFileName, "%s/%dsp_%dMs_%dB_%dlr_%s_%s", resultFolderName,
				numberOfSentPck, interTrnsmTime, packetSize, lr, filename, cmd_args->glblPars.currentTime);

	}

		
	
	
}

void time_to_string(struct cmd_args *cmd_args, char *tmbuf) {

	struct tm *nowtm;

	nowtm = localtime(&cmd_args->glblPars.test_time);
	strftime(tmbuf, 80, "%y%m%d%H%M", nowtm);
}

void mkpath(const char *dir) {
    char tmp[256];
    char *p = NULL;
    size_t len;
    int status = 1;
    snprintf(tmp, sizeof(tmp),"%s",dir);
    len = strlen(tmp);

    struct stat st = {0};

    if(tmp[len - 1] == '/')
        tmp[len - 1] = 0;

    for(p = tmp + 1; *p; p++)
        if(*p == '/') {
            *p = 0;
            if (stat(tmp, &st) == -1) {
            	status = mkdir(tmp, S_IRWXU);
			}            
            if (status == -1) {
    			//printf("Error Creating Sub Folder %s: %s\n\n",tmp, strerror(errno));
    			exit(-1);
    		}
            *p = '/';
        }

    mkdir(tmp, S_IRWXU);
    if (status == -1) {
    	//printf("Error Creating Sub Folder %s: %s\n\n",tmp, strerror(errno));
    	exit(-1);
    }
}

void tv2s(const struct timeval *tv, double *tValue) {
	*tValue = 0.0;
	
	*tValue = ((double)tv->tv_sec + (double)(tv->tv_usec) / 1000000.0);
	////printf("ReceiveTime %f\n", *tValue);
}

double tv2ms(const struct timeval *tv) {
	
	double tValue;
	tValue = (tv->tv_sec * 1000.0 + (tv->tv_usec/ 1000.0));

	return tValue;
}

double tv2micros(const struct timeval *tv) {
	
	double tValue = (tv->tv_sec * 1000000.0 + tv->tv_usec);

	return tValue;
}

void tv_divide(const unsigned long divisor, const struct timeval *tv, struct timeval *result)
{
	uint64_t x = ((uint64_t)tv->tv_sec * 1000 * 1000 + tv->tv_usec) / divisor;

	result->tv_sec = x / 1000 / 1000;
	result->tv_usec = x % (1000 * 1000);
}

void tv_scale(const unsigned long mult, const struct timeval *tv, struct timeval *result)
{
	result->tv_sec = mult * tv->tv_sec;
	result->tv_sec += tv->tv_usec * mult / 1000 / 1000;
	result->tv_usec = tv->tv_usec * mult % (1000 * 1000);
}

void skip(char **str, char s, char option) {
	if (**str != s) {
		//printf("Expected '%c' when parsing option '%c'\n", s, option);
		exit(1);
	}
	(*str)++;
}

long next_digit(char *str, char **endptr, long *result) {
	errno = 0;    /* To distinguish success/failure after call */
	*result = strtol(str, endptr, 10);

	/* Check for various possible errors */
	if ((errno == ERANGE && (*result == LONG_MAX || *result == LONG_MIN))
	    || (errno != 0 && *result == 0)) {
		return -1;
	}

	// No digits found
	if (*endptr == str) {
		return 0;
	}
	return 1;
}

long next_int(char **str) {
	char *endptr;
	long result;

	int ret = next_digit(*str, &endptr, &result);

	if (ret == -1) {
		fprintf(stderr, "Failed to parse '%s'\n", *str);
		perror("strtol");
		return -1;
	}
	else if (ret == 0) {
		fprintf(stderr, "No digits found in '%s'\n", *str);
		return -1;
	}

	*str = endptr;
	return result;
}

void deplete_sendbuffer(struct connectionInfo* ci) {
#ifdef __linux__
	int last_outstanding = -1;
	int outstanding = 0;
	int safety = 10;
	while (safety--) {
		ioctl(ci->sock, TIOCOUTQ, &outstanding);
		// 0 outstanding bytes left in buffer
		if (outstanding == 0) {
			break;
		}

		last_outstanding = outstanding;
		usleep(1000);
	}
#endif
}

int receiveMessage(int sock) {

	int max_sd, new_sd;
	int rc;
	int i, len;
	struct timeval timeout;
	fd_set master_set, reading_set;
	int on = 1;
	int received_number = 0;

	//Allow socket descriptor to be reuseable
	rc = setsockopt(sock, SOL_SOCKET,  SO_REUSEADDR,
	                (char *)&on, sizeof(on));
	if (rc < 0)
	{
		perror("setsockopt() failed");
		return ERROR;
	}

	// Set socket to be non-blocking.

	rc = ioctl(sock, FIONBIO, (char *)&on);
	if (rc < 0)
	{
		perror("ioctl() failed");
		return ERROR;
	}

	FD_ZERO(&master_set);
	max_sd = sock;
	FD_SET(sock, &master_set);

	timeout.tv_sec  = 50;
	timeout.tv_usec = 0;

	memcpy(&reading_set, &master_set, sizeof(master_set));
	rc = select(max_sd + 1, &reading_set, NULL, NULL, &timeout);

	if (rc == 0) {
		//printf("Select TimeOut!!!!\n");
		return ERROR;
	}

    /* read expected file size from server, 4 bytes unsigned int; network byte order ntohl (2^32 = max 4 GB)*/
	
    int res = recv(sock, &received_number, sizeof(received_number),0);
    if (res == 0) {
    	//printf("The Other Peer Has Shut Down!!!\n");
    	return -2;
    }
    
    if (res < 0) {
        perror("recvDecision: error getting receiver Message");
        return ERROR;
    }

    received_number = ntohl(received_number);

	return received_number;

}

int queueTime_lossDensity_plot(char *plotFileName, char *queueTime, 
	char *lossNr, char *lossDensity, int test_duration, 
	double max_qTime, double frstLssNr_Time, double maxPckNr_Time)
{
	double round_max_qTime = ceil(max_qTime * 1000);
	int rqt = (int) round_max_qTime;
	int five_remain = rqt % 5;
	rqt += (5 - five_remain);
	int yticsNr = rqt / 5;

	double xrange = (double) test_duration / 1000000.0;
	gnuplot_ctrl * g = gnuplot_init();
	gnuplot_cmd(g, "set terminal postscript eps enhanced color \"Courier\" 20");
	gnuplot_cmd(g, "set output \"%s\"", plotFileName);
	gnuplot_cmd(g, "set nokey");
	gnuplot_cmd(g, "set size 2");
	gnuplot_cmd(g, "set grid xtics ytics");

	gnuplot_cmd(g, "set object 2 rectangle from %f,0 to %f,%d fillcolor rgb \"#FFE4C4\" behind",
		frstLssNr_Time, maxPckNr_Time, 3000);
	
	gnuplot_cmd(g, "set ytics %d nomirror", yticsNr);
	gnuplot_cmd(g, "set ylabel 'Queueing Delay[ms]'");
	gnuplot_cmd(g, "set y2tics 20 nomirror");
	gnuplot_cmd(g, "set y2label 'Loss Density'");
	gnuplot_cmd(g, "set xlabel 'Sent Time[s]'");
	gnuplot_cmd(g, "set yrange [0:%d]", rqt);
	if (test_duration > 1000000)
		gnuplot_cmd(g, "set xtics 1");
	gnuplot_cmd(g, "set y2range [0:100]");
	gnuplot_cmd(g, "set origin 0,0");
	gnuplot_cmd(g, "set style line 1 lc rgb '#0060ad' lt 1 lw 5 pt 7 pi -1 ps 1.5");
	gnuplot_cmd(g, "plot \"%s\" using 2:($4 * 1000) with dots ls 1 lw 6 lc rgb \"blue\" title \"%s\",\\", 
		queueTime, DEFAULT_QUEUETIME_TITLE); 
	gnuplot_cmd(g, "\"%s\" using 1:2 with dots ls 1 lw 6 lc rgb \"black\" notitle axes x1y2, \\", 
		lossDensity); 
	gnuplot_cmd(g, "\"%s\" using 2:($4 * 0) with dots ls 1 lw 6 lc rgb \"%s\" title \"%s\"", lossNr, 
			DEFAULT_LOSSNUMBER_COLOR, DEFAULT_LOSSNUMBER_TITLE);
	gnuplot_close(g);

	return 0 ;
}